webpackJsonp([3],{

/***/ 681:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InformacionPageModule", function() { return InformacionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__informacion__ = __webpack_require__(696);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InformacionPageModule = /** @class */ (function () {
    function InformacionPageModule() {
    }
    InformacionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__informacion__["a" /* InformacionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__informacion__["a" /* InformacionPage */]),
            ],
        })
    ], InformacionPageModule);
    return InformacionPageModule;
}());

//# sourceMappingURL=informacion.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    AppSettings.EP_LOCAL = 'http://127.0.0.1:8000/';
    AppSettings.EP_VALLE = 'http://137.117.66.239/';
    //public static EP_VALLE = 'http://172.19.161.3:5050/';
    AppSettings.IGOB_CIUDADANO_FOTO = 'http://23.96.17.251:27017/';
    return AppSettings;
}());

//# sourceMappingURL=app.settings.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformacionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_app_settings__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(344);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the InformacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InformacionPage = /** @class */ (function () {
    function InformacionPage(navCtrl, navParams, httpService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpService = httpService;
        this.id_mas = navParams.get('id_mas');
        console.log("el id que llega", this.id_mas);
    }
    InformacionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InformacionPage');
        //  this.peticionToken();
        this.informacionmas();
        console.log("el id que llega", this.id_mas);
        this.holala = "";
    };
    InformacionPage.prototype.peticionToken = function () {
        var _this = this;
        // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
        var url = "http://172.19.161.3/api/apiLogin";
        var params = { "usr_usuario": "administrador", "usr_clave": "123456" };
        this.httpService.loginResource(url, params).
            subscribe(function (_token) { return _this.ayudaPeticionToken(_token); }, function (error) {
            alert(error);
        });
    };
    InformacionPage.prototype.ayudaPeticionToken = function (data) {
        if (typeof (data.token) != 'undefined') {
            localStorage.setItem('TOKEN_CAJERO', data.token);
        }
        console.log(data);
    };
    InformacionPage.prototype.volverprin = function () {
        this.navCtrl.push("InicioPage");
    };
    InformacionPage.prototype.infomas = function () {
        var _this = this;
        var zon_id = "2224946";
        var url = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
        var param = { "identificador": "SISTEMA_VALLE-1808", "parametros": '{"id_mascota":"198"}' };
        this.httpService.requestResource(url, param).subscribe(function (_data) {
            console.log("DATOS", _data);
            var var_aux = _data;
            if (_data != "[{ }]") {
                for (var i = 0; i < _data.length; i++) {
                    // if (var_aux[i].xvia_ae_data) {
                    var_aux[i].nombre_mas = (var_aux[i].nombre_mas);
                    // var_aux[i].xzona_data = JSON.parse (var_aux[i].xzona_data);
                    // }
                }
                _this.holala = JSON.stringify(var_aux[0].nombre_mas);
                console.log('epifanio', _this.holala);
                _this.data_listar = var_aux;
                console.log('comboooo via', _this.data_listar);
            }
        }, function (error) {
            console.log("DATOS");
            alert(error);
        });
        console.log('suer', this.holala);
    };
    InformacionPage.prototype.informacionmas = function () {
        var _this = this;
        var zon_id = "2224946";
        var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
        var param = { "identificador": "CEMENTERIO-5", "parametros": '{"id_mascota":"' + this.id_mas + '"}' };
        this.httpService.requestResource(url, param).subscribe(function (_data) {
            console.log("DATOS", _data);
            var aux_array = [];
            if (_data != "[{ }]") {
                for (var i = 0; i < _data.length; i++) {
                    if (_data[i].xmascota_data) {
                        aux_array.push(JSON.parse(_data[i].xmascota_data));
                    }
                    //  var_aux[i].xmascota_data = JSON.parse(var_aux[i].xmascota_data);
                }
                _this.data_listar = (aux_array);
                console.log('comboooo via', _this.data_listar);
            }
        }, function (error) {
            console.log("DATOS");
            alert(error);
        });
        console.log('suer', this.holala);
    };
    InformacionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-informacion',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/informacion/informacion.html"*/'<!--\n  Generated template for the InformacionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title>Informacion</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n<ion-list>\n<ion-item>\n     <ion-label>NOMBRE : \n      \n      </ion-label>\n\n     <ion-label *ngFor="let item of data_listar">\n       {{ item.nombre_mas }}      \n      </ion-label>\n \n      \n  \n \n</ion-item>\n<ion-item>\n     <ion-label>RAZA :\n      \n      </ion-label>\n\n     <ion-label *ngFor="let item of data_listar">\n       {{ item.raza }}      \n      </ion-label>\n \n      \n  \n \n</ion-item>\n<ion-item>\n     <ion-label>COLOR :\n      \n      </ion-label>\n\n     <ion-label *ngFor="let item of data_listar">\n       {{ item.color }}      \n      </ion-label>\n \n      \n  \n \n</ion-item>\n<ion-item>\n     <ion-label>SEXO :\n      \n      </ion-label>\n\n     <ion-label *ngFor="let item of data_listar">\n       {{ item.sexo }}      \n      </ion-label>\n \n      \n  \n \n</ion-item>\n<ion-item>\n     <ion-label>VETERINARIO ACTUAL :\n      \n      </ion-label>\n\n     <ion-label *ngFor="let item of data_listar">\n       {{ item.vete_actual }}      \n      </ion-label>\n \n      \n  \n \n</ion-item>\n\n  \n</ion-list>\n\n\n<button ion-button block round color="boton" (click) = " volverprin() ">\n    VOLVER\n  </button>\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/informacion/informacion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */]])
    ], InformacionPage);
    return InformacionPage;
}());

//# sourceMappingURL=informacion.js.map

/***/ })

});
//# sourceMappingURL=3.js.map