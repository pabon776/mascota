webpackJsonp([8],{

/***/ 683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrarDosPageModule", function() { return RegistrarDosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registrar_dos__ = __webpack_require__(698);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegistrarDosPageModule = /** @class */ (function () {
    function RegistrarDosPageModule() {
    }
    RegistrarDosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__registrar_dos__["a" /* RegistrarDosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registrar_dos__["a" /* RegistrarDosPage */]),
            ],
        })
    ], RegistrarDosPageModule);
    return RegistrarDosPageModule;
}());

//# sourceMappingURL=registrar-dos.module.js.map

/***/ }),

/***/ 698:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrarDosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RegistrarDosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegistrarDosPage = /** @class */ (function () {
    function RegistrarDosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RegistrarDosPage.prototype.abrirCompromiso = function () {
        this.navCtrl.push("CompromimisoPage");
        //  this.navCtrl.push(InicioPage);
    };
    RegistrarDosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistrarDosPage');
    };
    RegistrarDosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registrar-dos',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/registrar-dos/registrar-dos.html"*/'<!--\n  Generated template for the RegistrarDosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title>RegistrarDos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n <ion-label align="center"> REGISTRAR DATOS DE SALUD </ion-label>\n\n<ion-grid>\n   <ion-row >\n    <ion-col  col-5>\n      <ion-label align="left">\n        ESTERILIZACION :\n      </ion-label>\n    </ion-col>\n      <ion-col col-7>\n      <ion-list>\n<ion-item>\n\n    <ion-select  selected="true">\n      <ion-option>NO</ion-option>\n      <ion-option >SI</ion-option>\n\n    </ion-select>\n</ion-item>\n</ion-list>\n</ion-col>\n  </ion-row>\n  </ion-grid>\n\n\n  <ion-grid>\n <ion-row center full color="primary">\n    <ion-col width-50 left col-5>\n      <ion-label>\n        NOMBRE DEL VETERINARIO (El que realizo la esterilizacion) :\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 center col-7>\n<ion-input type="text" color="primary" color="dark" round ></ion-input>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n  <ion-grid>\n <ion-row center full color="primary">\n    <ion-col width-50 left col-5>\n      <ion-label>\n        NOMBRE DEL VETERINARIO (Actual) :\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 center col-7>\n<ion-input type="text" color="primary" color="dark" round ></ion-input>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n<ion-grid>\n   <ion-row >\n    <ion-col  col-5>\n      <ion-label align="left">\n        VACUNAS :\n      </ion-label>\n    </ion-col>\n      <ion-col col-7>\n      <ion-list>\n<ion-item>\n\n    <ion-select  selected="true">\n      <ion-option>NO</ion-option>\n      <ion-option >SI</ion-option>\n\n    </ion-select>\n</ion-item>\n</ion-list>\n</ion-col>\n  </ion-row>\n  </ion-grid>\n\n\n\n\n  <ion-grid>\n <ion-row center full color="primary">\n    <ion-col width-50 left col-5>\n      <ion-label >\n        ADJUNTAR LA FOTO DE LA MASCOTA :\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 center col-7>\n<ion-input type="text" color="primary" color="dark" round ></ion-input>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n<button ion-button block round color="boton" (click) = " abrirCompromiso() ">\n    CONTINUAR\n  </button>\n\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/registrar-dos/registrar-dos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], RegistrarDosPage);
    return RegistrarDosPage;
}());

//# sourceMappingURL=registrar-dos.js.map

/***/ })

});
//# sourceMappingURL=8.js.map