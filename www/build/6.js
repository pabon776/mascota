webpackJsonp([6],{

/***/ 687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacunasPageModule", function() { return VacunasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__vacunas__ = __webpack_require__(702);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var VacunasPageModule = /** @class */ (function () {
    function VacunasPageModule() {
    }
    VacunasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__vacunas__["a" /* VacunasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__vacunas__["a" /* VacunasPage */]),
            ],
        })
    ], VacunasPageModule);
    return VacunasPageModule;
}());

//# sourceMappingURL=vacunas.module.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VacunasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the VacunasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.kjhlzdzgsffzf
 */
var VacunasPage = /** @class */ (function () {
    function VacunasPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.nombre = navParams.get('nombre');
        this.especie = navParams.get('especie');
        this.raza = navParams.get('raza');
        this.sexo = navParams.get('sexo');
        this.peso = navParams.get('peso');
        this.anios = navParams.get('anios');
        this.tamano = navParams.get('tamano');
        this.color = navParams.get('color');
        this.vacunas = navParams.get('vacunas');
        this.marca = navParams.get('marca');
        this.vete_actual = navParams.get('vete_actual');
        this.peligro = navParams.get('peligro');
        this.desparacitacion = navParams.get('desparacitacion');
        var lol = this.nombre + "  bolvia " + this.vete_actual;
        console.log("hola", lol);
        console.log("hola", this.peso);
        console.log("hola", this.vacunas);
        console.log("hola", this.marca);
        console.log("esta es la desparacitacion en vacunas", this.desparacitacion);
        this.datovac = "";
    }
    VacunasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VacunasPage');
    };
    VacunasPage.prototype.acumulacionVacuna = function () {
        if (this.tipo_vacuna === "" || this.nro_dosis === "" || this.nomb_vete === "" || this.inst_vete === "" || this.tipo_vacuna === undefined || this.nro_dosis === undefined || this.nomb_vete === undefined || this.inst_vete === undefined) {
            this.showToastWithCloseButton();
        }
        else {
            var jason = '{"tipo_vacuna":"' + this.tipo_vacuna + '","nro_dosis":"' + this.nro_dosis + '","nomb_vete":"' + this.nomb_vete + '","inst_vete":"' + this.inst_vete + '"},';
            this.datovac = this.datovac + jason;
            console.log(this.datovac);
            this.tipo_vacuna = "";
            this.nro_dosis = "";
            this.nomb_vete = "";
            this.inst_vete = "";
            jason = '';
            this.showToastDos();
        }
    };
    VacunasPage.prototype.abrirCompromiso = function () {
        if (this.tipo_vacuna === undefined || this.nro_dosis === undefined || this.nomb_vete === undefined || this.inst_vete === undefined) {
            this.showToastWithCloseButton();
        }
        else {
            var jo1 = this.datovac.substring(0, this.datovac.length - 1);
            //var jo1 = this.datovac+'{}';
            var reg_vacunas = '[' + jo1 + ']';
            console.log(reg_vacunas);
            //  var reg_vacunas = '[{"tipo_vacuna":"'+this.tipo_vacuna+'","nro_dosis":"'+this.nro_dosis+'","nomb_vete":"'+this.nomb_vete+'","inst_vete":"'+this.inst_vete+'"}]';
            //    console.log(reg_vacunas);
            if (this.marca === 'ninguna') {
                if (this.desparacitacion === 'si') {
                    this.navCtrl.setRoot("DesparacicitacionPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: reg_vacunas, desparacitacion: this.desparacitacion });
                }
                else {
                    this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: reg_vacunas, desparacitacion: this.desparacitacion });
                }
            }
            if (this.marca === 'certificado') {
                this.navCtrl.setRoot("CertificadoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: reg_vacunas, desparacitacion: this.desparacitacion });
            }
            if (this.marca === 'tatuaje') {
                this.navCtrl.setRoot("TatuajePage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: reg_vacunas, desparacitacion: this.desparacitacion });
            }
            if (this.marca === 'chip') {
                this.navCtrl.setRoot("ChipPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: reg_vacunas, desparacitacion: this.desparacitacion });
            }
        }
    };
    VacunasPage.prototype.abrirVacunas = function () {
        this.navCtrl.push("VacunasPage");
        //  this.navCtrl.push(InicioPage);xzcxf 
    };
    VacunasPage.prototype.showToastWithCloseButton = function () {
        var toast = this.toastCtrl.create({
            message: 'Debe llenar todos los campos',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    VacunasPage.prototype.showToastDos = function () {
        var toast = this.toastCtrl.create({
            message: 'Vacuna Registrada',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    VacunasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-vacunas',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/vacunas/vacunas.html"*/'<!--\n  Generated template for the VacunasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title align="center">REGISTRAR VACUNAS</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n<ion-list>\n\n\n<ion-item>\n      <ion-label>\n        TIPO DE VACUNA :\n      </ion-label>\n\n    <ion-select  [(ngModel)] =" tipo_vacuna ">\n      <ion-option value="antirrabica nacional">ANTIRRABICA NACIONAL</ion-option>\n      <ion-option value="antirrabica americana">ANTIRRABICA AMERICANA</ion-option>\n      <ion-option value="octuple">OCTUPLE</ion-option>\n      <ion-option value="triple felina">TRIPLE FELINA</ion-option>\n\n    </ion-select>\n</ion-item>\n\n\n  <ion-item>\n    <ion-label stacked>NRO DE DOSIS :</ion-label>\n   <ion-input type="text"  round [(ngModel)] =" nro_dosis "></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>NOMBRE DEL VETERINARIO :</ion-label>\n<ion-input type="text" round [(ngModel)] =" nomb_vete "></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>INSTITUCION VETERINARIA :</ion-label>\n<ion-input type="text" round [(ngModel)] =" inst_vete "></ion-input>\n  </ion-item>\n\n\n\n\n\n</ion-list>\n\n\n  <button ion-button block round color="boton" (click) = " acumulacionVacuna() ">\n    REGISTRAR\n  </button>\n\n  <button ion-button block round color="boton" (click) = " abrirCompromiso() ">\n    CONTINUAR\n  </button>\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/vacunas/vacunas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], VacunasPage);
    return VacunasPage;
}());

//# sourceMappingURL=vacunas.js.map

/***/ })

});
//# sourceMappingURL=6.js.map