webpackJsonp([2],{

/***/ 680:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioPageModule", function() { return InicioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inicio__ = __webpack_require__(695);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InicioPageModule = /** @class */ (function () {
    function InicioPageModule() {
    }
    InicioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__inicio__["a" /* InicioPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__inicio__["a" /* InicioPage */]),
            ],
        })
    ], InicioPageModule);
    return InicioPageModule;
}());

//# sourceMappingURL=inicio.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    AppSettings.EP_LOCAL = 'http://127.0.0.1:8000/';
    AppSettings.EP_VALLE = 'http://137.117.66.239/';
    //public static EP_VALLE = 'http://172.19.161.3:5050/';
    AppSettings.IGOB_CIUDADANO_FOTO = 'http://23.96.17.251:27017/';
    return AppSettings;
}());

//# sourceMappingURL=app.settings.js.map

/***/ }),

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InicioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_app_settings__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(344);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//*********ERICKA


//import { Http, Headers, RequestOptions, Response } from '@angular/http';
//**************ERICKA
/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InicioPage = /** @class */ (function () {
    function InicioPage(navCtrl, navParams, httpService, alerCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpService = httpService;
        this.alerCtrl = alerCtrl;
        console.log("holap ");
    }
    InicioPage_1 = InicioPage;
    InicioPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InicioPage');
        this.peticionToken();
    };
    InicioPage.prototype.abrirMascota = function () {
        this.navCtrl.push("MascotaPage");
        //  this.navCtrl.push(InicioPage);
    };
    InicioPage.prototype.abrirVacunas = function () {
        this.navCtrl.push("VacunasPage");
        //  this.navCtrl.push(InicioPage);
    };
    InicioPage.prototype.abrirhome = function () {
        // this.navCtrl.push("HomePage");
        //nav.popToRoot();
        this.navCtrl.push(InicioPage_1);
    };
    InicioPage.prototype.abrirContfacto = function () {
        var _this = this;
        var confirm = this.alerCtrl.create({
            title: 'ADVERTENCIA',
            message: 'El contacto alternativo debe ser mayor de edad , desea continuar ?',
            buttons: [
                {
                    text: 'CANCEL',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'ACEPTAR',
                    handler: function () {
                        console.log('Agree clicked');
                        _this.navCtrl.push("ContactosPage");
                    }
                }
            ]
        });
        confirm.present();
    };
    InicioPage.prototype.peticionTokendos = function () {
        var _this = this;
        // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
        var url = "http://192.168.5.141:9097/wsIf/crearCasoAeLinea";
        var erikita = '{"INT_ID_CAT_AGRUPADA":85,"INT_AC_MACRO_ID":5}';
        var params = { "usr_id": 1, "datos": "{\"INT_ID_CAT_AGRUPADA\":85,\"INT_AC_MACRO_ID\":5}", "procodigo": "AER-LICEN" };
        this.httpService.loginResource(url, params).
            subscribe(function (_token) { return _this.ayudaPeticionTokeno(_token); }, function (error) {
            alert(error);
        });
    };
    InicioPage.prototype.ayudaPeticionTokeno = function (data) {
        console.log('resultado', data);
    };
    InicioPage.prototype.peticionToken = function () {
        var _this = this;
        // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
        var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + "api/apiLogin";
        var params = { "usr_usuario": "motores.cementerio", "usr_clave": "motoresUdit" };
        this.httpService.loginResource(url, params).
            subscribe(function (_token) { return _this.ayudaPeticionToken(_token); }, function (error) {
            alert(error);
        });
    };
    InicioPage.prototype.ayudaPeticionToken = function (data) {
        if (typeof (data.token) != 'undefined') {
            localStorage.setItem('TOKEN_CAJERO', data.token);
        }
        console.log(data);
    };
    InicioPage = InicioPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-inicio',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/inicio/inicio.html"*/'<!--\n  Generated template for the InicioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title >MENU</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding full color="rojo">\n\n  <div padding>\n    <button ion-button round block color="boton" (click) = " abrirMascota() ">\n     <ion-icon name="paw" item-start>  MIS MASCOTA </ion-icon>  </button>\n  </div>\n\n    <div padding>\n    <button ion-button  round block color="boton" (click) = " abrirContfacto() ">\n     <ion-icon name="paw" item-start>  CONTACTO ALTERNATIVO </ion-icon>  </button>\n  </div>\n   <!--   <div padding>\n    <button ion-button  block round color="boton" >\n     <ion-icon name="paw" item-start>  NOTIFICACIONES </ion-icon>  </button>\n  </div>\n      <div padding>\n    <button ion-button  block round color="boton">\n     <ion-icon name="paw" item-start>  VOLVER </ion-icon>  </button>\n  </div>-->\n      <div padding>\n    <button ion-button  block round color="boton" >\n     <ion-icon name="paw" item-start>  SALIR </ion-icon>  </button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/inicio/inicio.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], InicioPage);
    return InicioPage;
    var InicioPage_1;
}());

//# sourceMappingURL=inicio.js.map

/***/ })

});
//# sourceMappingURL=2.js.map