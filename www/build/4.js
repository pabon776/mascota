webpackJsonp([4],{

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactosPageModule", function() { return ContactosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contactos__ = __webpack_require__(694);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactosPageModule = /** @class */ (function () {
    function ContactosPageModule() {
    }
    ContactosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contactos__["a" /* ContactosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contactos__["a" /* ContactosPage */]),
            ],
        })
    ], ContactosPageModule);
    return ContactosPageModule;
}());

//# sourceMappingURL=contactos.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    AppSettings.EP_LOCAL = 'http://127.0.0.1:8000/';
    AppSettings.EP_VALLE = 'http://137.117.66.239/';
    //public static EP_VALLE = 'http://172.19.161.3:5050/';
    AppSettings.IGOB_CIUDADANO_FOTO = 'http://23.96.17.251:27017/';
    return AppSettings;
}());

//# sourceMappingURL=app.settings.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_app_settings__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(344);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**


 * Generated class for the ContactosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ContactosPage = /** @class */ (function () {
    function ContactosPage(navCtrl, navParams, httpService, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpService = httpService;
        this.toastCtrl = toastCtrl;
    }
    ContactosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactosPage');
    };
    ContactosPage.prototype.registrar = function () {
        this.registrarContactoDos();
        //this.registrarContacto();
        //  this.volverhDVyInicio();
    };
    ContactosPage.prototype.volverhDVyInicio = function () {
        this.navCtrl.push("InicioPage");
        //  this.navCtrl.push(InicioPage);
    };
    ContactosPage.prototype.registrarContacto = function () {
        // var var_jason = JSON.stringify( Object.create(null, { x: { value: 'x', enumerable: false }, y: { value: 'y', enumerable: true } }) );
        var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
        var param = { "identificador": "SISTEMA_VALLE-1502", "parametros": '{"xcontacto_usr_id":123,"xcontacto_nombre":"' + this.nombre_ca + '","xcontacto_paterno":"' + this.apaterno_ca + '","xcontacto_materno":"' + this.apmaterno_ca + '","xcontacto_casada":"' + this.apcasada_ca + '","xcontacto_email":"' + this.email_ca + '","xcontacto_telefono":' + this.fijo_ca + ',"xcontacto_celular":' + this.celular_ca + '}' };
        this.httpService.requestResource(url, param).
            subscribe(function (_data) {
            console.log("DATOS", _data);
        }, function (error) {
            console.log("DATOS");
            alert(error);
        });
    };
    ContactosPage.prototype.registrarContactoDos = function () {
        var _this = this;
        if (this.nombre_ca === undefined || this.apaterno_ca === undefined || this.apmaterno_ca === undefined || this.email_ca === undefined || this.fijo_ca === undefined || this.celular_ca === undefined) {
            this.showToastWithCloseButton();
        }
        else {
            var var_jason = JSON.stringify(Object.create(null, { x: { value: 'x', enumerable: true }, y: { value: 'y', enumerable: true } }));
            //var dataEnvio  ='{"nombre_ca":"'+this.nombre_ca+'","apaterno_ca":"'this.apaterno_ca'"}';
            var dataEnvio = '{"nombre_ca": "' + this.nombre_ca + '","apaterno_ca": "' + this.apaterno_ca + '","apmaterno_ca":"' + this.apmaterno_ca + '","apcasada_ca":"' + this.apcasada_ca + '","email_ca":"' + this.email_ca + '","fijo_ca":"' + this.fijo_ca + '","celular_ca":"' + this.celular_ca + '"}';
            console.log("objeto: ", dataEnvio);
            console.log(var_jason);
            var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
            var param = { "identificador": "CASA_MASCOTA-4", "parametros": '{"xcontactos_usr_id":123,"xcontactos_data":' + JSON.stringify(dataEnvio) + '}' };
            this.httpService.requestResource(url, param).
                subscribe(function (_data) {
                console.log("DATOS", _data);
                _this.showToast();
            }, function (error) {
                console.log("DATOS");
                alert(error);
            });
            this.volverhDVyInicio();
        }
    };
    ContactosPage.prototype.showToastWithCloseButton = function () {
        var toast = this.toastCtrl.create({
            message: 'Debe llenar todos los campos',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    ContactosPage.prototype.showToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Registro Satisfactorio',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    ContactosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contactos',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/contactos/contactos.html"*/'<!--\n  Generated template for the ContactosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title>REGISTRAR CONTACTO</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n<ion-list>\n\n  <ion-item>\n    <ion-label stacked>NOMBRES :</ion-label>\n    <ion-input type="text" color="edit" round [(ngModel)] =" nombre_ca "></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>AP. PATERNO :</ion-label>\n    <ion-input type="text" color="edit" round [(ngModel)]=" apaterno_ca "></ion-input>\n  </ion-item>\n\n    <ion-item>\n    <ion-label stacked>AP. MATERNO :</ion-label>\n    <ion-input type="text" color="edit" round [(ngModel)]= " apmaterno_ca "></ion-input>\n  </ion-item>\n\n      <ion-item>\n    <ion-label stacked>AP. CASADO :</ion-label>\n    <ion-input type="text" color="edit" round [(ngModel)]= " apcasada_ca " ></ion-input>\n  </ion-item>\n\n        <ion-item>\n    <ion-label stacked>EMAIL :</ion-label>\n  <ion-input type="text" color="edit" round [(ngModel)] =" email_ca "></ion-input>\n  </ion-item>\n\n        <ion-item>\n    <ion-label stacked>FIJO :</ion-label>\n <ion-input type="text" color="edit" round [(ngModel)]=" fijo_ca " ></ion-input>\n  </ion-item>\n\n        <ion-item>\n    <ion-label stacked>CELULAR :</ion-label>\n <ion-input type="text" color="edit" round [(ngModel)]=" celular_ca "></ion-input>\n  </ion-item>\n\n\n</ion-list>\n\n\n\n      <div padding>\n    <button ion-button  block round color="boton" (click)=" registrar() "  >\n     <ion-icon name="paw" item-start>  REGISTRAR </ion-icon>  </button>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/contactos/contactos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], ContactosPage);
    return ContactosPage;
}());

//# sourceMappingURL=contactos.js.map

/***/ })

});
//# sourceMappingURL=4.js.map