webpackJsonp([13],{

/***/ 674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActualizarvacunaPageModule", function() { return ActualizarvacunaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__actualizarvacuna__ = __webpack_require__(689);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ActualizarvacunaPageModule = /** @class */ (function () {
    function ActualizarvacunaPageModule() {
    }
    ActualizarvacunaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__actualizarvacuna__["a" /* ActualizarvacunaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__actualizarvacuna__["a" /* ActualizarvacunaPage */]),
            ],
        })
    ], ActualizarvacunaPageModule);
    return ActualizarvacunaPageModule;
}());

//# sourceMappingURL=actualizarvacuna.module.js.map

/***/ }),

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActualizarvacunaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(344);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ActualizarvacunaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ActualizarvacunaPage = /** @class */ (function () {
    function ActualizarvacunaPage(navCtrl, navParams, httpService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpService = httpService;
    }
    ActualizarvacunaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ActualizarvacunaPage');
        this.peticionToken();
    };
    ActualizarvacunaPage.prototype.peticionToken = function () {
        var _this = this;
        // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
        var url = "http://172.19.161.3/api/apiLogin";
        var params = { "usr_usuario": "administrador", "usr_clave": "123456" };
        this.httpService.loginResource(url, params).
            subscribe(function (_token) { return _this.ayudaPeticionToken(_token); }, function (error) {
            alert(error);
        });
    };
    ActualizarvacunaPage.prototype.ayudaPeticionToken = function (data) {
        if (typeof (data.token) != 'undefined') {
            localStorage.setItem('TOKEN_CAJERO', data.token);
        }
        console.log(data);
    };
    ActualizarvacunaPage.prototype.infomasdos = function () {
        var url = 'http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
        // let param: any = {"identificador":"SISTEMA_VALLE-1809","parametros":'{"xmascotas_id":1,"xmascotas_nombre":"nnn","xmascotas_especie":"nnn","xmascotas_raza":"bnbn","xmascotas_sexo":"nmbn","xmascotas_peso":"nnmnm","xmascotas_edad":5,"xmascotas_tamanio":"grande","xmascotas_color":"negro","xmascotas_marca":"ninguna","xmascotas_veterinario_actual":"nmn","xmascotas_veterinario_actual":"nmn","xmascotas_usr_id":1}'}
        var param = { "identificador": "SISTEMA_VALLE-1809", "parametros": '{"xmascotas_id":1,"xmascotas_nombre":"nn","xmascotas_especie":"nnn","xmascotas_raza":"bnbn","xmascotas_sexo":"nmbn","xmascotas_peso":"nnmnm","xmascotas_edad":5,"xmascotas_tamanio":"grande","xmascotas_color":"negro","xmascotas_marca":"ninguna","xmascotas_veterinario_actual":"nmn","xmascotas_veterinario_actual":"nmn","xmascotas_usr_id":1,"xmascotas_vacuna":"nmnm"}' };
        this.httpService.requestResource(url, param).
            subscribe(function (_data) {
            console.log("DATOS", _data);
        }, function (error) {
            console.log("DATOS");
            alert(error);
        });
    };
    ActualizarvacunaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-actualizarvacuna',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/actualizarvacuna/actualizarvacuna.html"*/'<!--\n  Generated template for the ActualizarvacunaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header >\n\n  <ion-navbar full color ="rojo">\n    <ion-title>Actualizarvacuna</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-list>\n\n\n<ion-item>\n      <ion-label>\n        TIPO DE VACUNA :\n      </ion-label>\n\n    <ion-select  [(ngModel)] =" tipo_vacuna1 ">\n      <ion-option value="antirrabica nacional">ANTIRRABICA NACIONAL</ion-option>\n      <ion-option value="antirrabica americana">ANTIRRABICA AMERICANA</ion-option>\n      <ion-option value="octuple">OCTUPLE</ion-option>\n      <ion-option value="triple felina">TRIPLE FELINA</ion-option>\n\n    </ion-select>\n</ion-item>\n\n\n  <ion-item>\n    <ion-label stacked>NRO DE DOSIS :</ion-label>\n   <ion-input type="text"  round [(ngModel)] =" nro_dosis1 "></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>NOMBRE DEL VETERINARIO :</ion-label>\n<ion-input type="text" round [(ngModel)] =" nomb_vete1 "></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>INSTITUCION VETERINARIA :</ion-label>\n<ion-input type="text" round [(ngModel)] =" inst_vete1 "></ion-input>\n  </ion-item>\n\n</ion-list>\n\n  <button ion-button block round color="boton" (click) = " infomasdos() ">\n    REGISTRAR\n  </button>\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/actualizarvacuna/actualizarvacuna.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]])
    ], ActualizarvacunaPage);
    return ActualizarvacunaPage;
}());

//# sourceMappingURL=actualizarvacuna.js.map

/***/ })

});
//# sourceMappingURL=13.js.map