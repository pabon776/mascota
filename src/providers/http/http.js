var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
// NOTICE: Delete map and MessageProvider, maybe use retry.
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
var HttpProvider = /** @class */ (function () {
    function HttpProvider(http) {
        this.http = http;
    }
    HttpProvider.prototype.loginResource = function (url, param) {
        console.log("");
        return this.http.post(url, param)
            .pipe(tap(function (data) { return data; }), catchError(this.handleError));
    };
    HttpProvider.prototype.requestResource = function (url, param) {
        return this.http.post(url, param, this.jwt())
            .pipe(tap(function (data) { return data; }), catchError(this.handleError));
    };
    HttpProvider.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('Ocurrió un error:', error.error.message);
            return Observable.throw("Error al procesar la solicitud (client-netework error) " + error);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend codigo de retorno " + error.status + ", " +
                ("El cuerpo fue: " + JSON.parse(JSON.stringify(error.error))));
            return Observable.throw("Error al procesar la solicitud (server error) " + JSON.stringify(error.error));
        }
        // return Observable.throw (`Error al procesar la solicitud: ${error}`);
    };
    HttpProvider.prototype.jwt = function () {
        // create authorization header with jwt token
        var currentUser = JSON.parse(localStorage.getItem('TOKEN_CAJERO'));
        if (currentUser && currentUser.token) {
            var httpOptions_1 = {
                headers: new HttpHeaders({
                    'Authorization': 'Bearer ' + currentUser.token
                })
            };
            return httpOptions_1;
        }
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'my-auth-token'
            })
        };
        /*let token_falso: string = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hcGkvYXBpTG9naW4iLCJpYXQiOjE1NDE1ODM2NjgsImV4cCI6MTU0MTU4NzI2OCwibmJmIjoxNTQxNTgzNjY4LCJqdGkiOiJXYXZzR2ltZjNwbUY4WFZIIn0.ER6RermBbU-UQ7f8EC1Nmra69gd-XOGI80LcEMFOgvM`;
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': 'Bearer ' + token_falso
          })
        };

        return httpOptions;*/
    };
    HttpProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], HttpProvider);
    return HttpProvider;
}());
export { HttpProvider };
//# sourceMappingURL=http.js.map