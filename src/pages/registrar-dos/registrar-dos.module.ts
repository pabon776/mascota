import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrarDosPage } from './registrar-dos';

@NgModule({
  declarations: [
    RegistrarDosPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrarDosPage),
  ],
})
export class RegistrarDosPageModule {}
