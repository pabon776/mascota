import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';



import { AppSettings } from '../../config/app.settings';
import { HttpProvider } from '../../providers/http/http';
/**


 * Generated class for the ContactosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contactos',
  templateUrl: 'contactos.html',
})
export class ContactosPage {

 private nombre_ca : string;
private apaterno_ca : string;
 private apmaterno_ca : string;
private apcasada_ca : string;
private email_ca : string;
 private fijo_ca : string;
 private celular_ca : string;
  private data1 : string;

  constructor(public navCtrl: NavController, public navParams: NavParams,private httpService: HttpProvider, public toastCtrl: ToastController  ) {
 

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactosPage');

  }


  public registrar ():void {


    this.registrarContactoDos();
    //this.registrarContacto();
  //  this.volverhDVyInicio();


  }
        volverhDVyInicio() {
   this.navCtrl.push("InicioPage");
   //  this.navCtrl.push(InicioPage);
  }


    public registrarContacto () {

    // var var_jason = JSON.stringify( Object.create(null, { x: { value: 'x', enumerable: false }, y: { value: 'y', enumerable: true } }) );

   let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"SISTEMA_VALLE-1502","parametros":'{"xcontacto_usr_id":123,"xcontacto_nombre":"'+this.nombre_ca+'","xcontacto_paterno":"'+this.apaterno_ca+'","xcontacto_materno":"'+this.apmaterno_ca+'","xcontacto_casada":"'+this.apcasada_ca+'","xcontacto_email":"'+this.email_ca+'","xcontacto_telefono":'+this.fijo_ca+',"xcontacto_celular":'+this.celular_ca+'}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });
  }

    public registrarContactoDos () {


      if (this.nombre_ca===undefined || this.apaterno_ca===undefined || this.apmaterno_ca===undefined || this.email_ca===undefined || this.fijo_ca===undefined || this.celular_ca===undefined) {

          this.showToastWithCloseButton();

      }else{

    let var_jason = JSON.stringify( Object.create(null, { x: { value: 'x', enumerable: true }, y: { value: 'y', enumerable: true } }) );

     //var dataEnvio  ='{"nombre_ca":"'+this.nombre_ca+'","apaterno_ca":"'this.apaterno_ca'"}';
   


    var dataEnvio  ='{"nombre_ca": "'+this.nombre_ca+'","apaterno_ca": "'+this.apaterno_ca+'","apmaterno_ca":"'+this.apmaterno_ca+'","apcasada_ca":"'+this.apcasada_ca+'","email_ca":"'+this.email_ca+'","fijo_ca":"'+this.fijo_ca+'","celular_ca":"'+this.celular_ca+'"}';

    console.log("objeto: ",dataEnvio);  
        console.log(var_jason);
   let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CASA_MASCOTA-4","parametros":'{"xcontactos_usr_id":123,"xcontactos_data":'+JSON.stringify(dataEnvio)+'}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);
                this.showToast();

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });

          this.volverhDVyInicio();



      }




  }


        showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Debe llenar todos los campos',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }
      showToast() {
    const toast = this.toastCtrl.create({
      message: 'Registro Satisfactorio',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }


}
