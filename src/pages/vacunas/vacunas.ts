import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController } from 'ionic-angular';
import { CompromimisoPage } from '../compromimiso/compromimiso';
import { CertificadoPage } from '../certificado/certificado';
import { TatuajePage } from '../tatuaje/tatuaje';
import { ChipPage } from '../chip/chip';

import { DesparacicitacionPage } from '../desparacicitacion/desparacicitacion';

/**
 * Generated class for the VacunasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.kjhlzdzgsffzf
 */

@IonicPage()
@Component({
  selector: 'page-vacunas',
  templateUrl: 'vacunas.html',
})
export class VacunasPage {

    nombre: string; 
    especie: string; 
    raza: string; 
    sexo: string; 
    peso: string; 
    anios: string; 
    tamano: string; 
    color: string; 
    vacunas: string; 
    marca: string; 
    vete_actual: string; 
    peligro: string;  
     desparacitacion: string; 




private tipo_vacuna: string;
private nro_dosis: string;
private nomb_vete: string;
private inst_vete: string;
private datovac: string;


  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController ) {

    this.nombre=navParams.get('nombre');
    this.especie=navParams.get('especie');
    this.raza=navParams.get('raza');
    this.sexo=navParams.get('sexo');
    this.peso=navParams.get('peso');
    this.anios=navParams.get('anios');
    this.tamano=navParams.get('tamano');
    this.color=navParams.get('color');
    this.vacunas=navParams.get('vacunas');
    this.marca=navParams.get('marca');
    this.vete_actual=navParams.get('vete_actual');
    this.peligro=navParams.get('peligro');
    this.desparacitacion=navParams.get('desparacitacion');

    var lol = this.nombre+"  bolvia "+this.vete_actual;
    console.log("hola",lol);
     console.log("hola",this.peso);
      console.log("hola",this.vacunas);
         console.log("hola",this.marca);

           console.log("esta es la desparacitacion en vacunas",this.desparacitacion);

           this.datovac="";


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VacunasPage');
  }


  acumulacionVacuna(){
        if (this.tipo_vacuna==="" || this.nro_dosis==="" || this.nomb_vete==="" || this.inst_vete==="" || this.tipo_vacuna===undefined || this.nro_dosis===undefined || this.nomb_vete===undefined || this.inst_vete===undefined) {
          this.showToastWithCloseButton();
        }else{
        var jason = '{"tipo_vacuna":"'+this.tipo_vacuna+'","nro_dosis":"'+this.nro_dosis+'","nomb_vete":"'+this.nomb_vete+'","inst_vete":"'+this.inst_vete+'"},';
        this.datovac=this.datovac+jason;  
        console.log(this.datovac);
        this.tipo_vacuna="";
        this.nro_dosis="";
        this.nomb_vete="";
        this.inst_vete="";
        jason = '';

        this.showToastDos();

        }


  }


      abrirCompromiso() {


        if (this.tipo_vacuna===undefined || this.nro_dosis===undefined || this.nomb_vete===undefined || this.inst_vete===undefined) {

            this.showToastWithCloseButton();


        }else {

          let jo1 = this.datovac.substring(0, this.datovac.length-1);
          //var jo1 = this.datovac+'{}';
          var reg_vacunas='['+jo1+']';
          console.log(reg_vacunas);

              //  var reg_vacunas = '[{"tipo_vacuna":"'+this.tipo_vacuna+'","nro_dosis":"'+this.nro_dosis+'","nomb_vete":"'+this.nomb_vete+'","inst_vete":"'+this.inst_vete+'"}]';
            //    console.log(reg_vacunas);
                      if (this.marca === 'ninguna') {

                        if (this.desparacitacion==='si') {

                                      this.navCtrl.setRoot("DesparacicitacionPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:reg_vacunas,desparacitacion:this.desparacitacion});
      
                        }else{

                                       this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:reg_vacunas,desparacitacion:this.desparacitacion});
                 
                        }


                    
                      }
                            if (this.marca === 'certificado') {


                                       this.navCtrl.setRoot("CertificadoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:reg_vacunas,desparacitacion:this.desparacitacion});
                 
                      }
                            if (this.marca === 'tatuaje') {


                                       this.navCtrl.setRoot("TatuajePage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:reg_vacunas,desparacitacion:this.desparacitacion});
                 
                      }
                            if (this.marca === 'chip') {


                                       this.navCtrl.setRoot("ChipPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:reg_vacunas,desparacitacion:this.desparacitacion});
                 
                      }

        }




  }
            abrirVacunas() {
   this.navCtrl.push("VacunasPage");
   //  this.navCtrl.push(InicioPage);xzcxf 

   
  }

  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Debe llenar todos los campos',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

        showToastDos() {
    const toast = this.toastCtrl.create({
      message: 'Vacuna Registrada',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

}
