import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';
import { MascotaPage } from '../mascota/mascota';

import { AppSettings } from '../../config/app.settings';
import { HttpProvider } from '../../providers/http/http';
/**
 * Generated class for the InformacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-informacion',
  templateUrl: 'informacion.html',
})
export class InformacionPage {
 private data_listar: string;
  private nommascota: string;
  private nombremascota: string;
  private holala: string;
  private razazz: string;
  private id_mas:string;
  private picture:string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams , private httpService: HttpProvider) {
  	this.id_mas=navParams.get('id_mas');
  	console.log("el id que llega",this.id_mas);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformacionPage');
  //  this.peticionToken();
  this.informacionmas();
  console.log("el id que llega",this.id_mas);
    this.holala="";
  }

  public peticionToken (): void {
    // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
    let url: string ="http://172.19.161.3/api/apiLogin";
    let params: any = {"usr_usuario": "administrador", "usr_clave": "123456"};
    this.httpService.loginResource (url, params).
        subscribe (
              _token => this.ayudaPeticionToken (_token),
              error => {
                alert(error);
              });
  }

  public ayudaPeticionToken (data) {
      if ( typeof(data.token) != 'undefined' ) {
      localStorage.setItem ('TOKEN_CAJERO', data.token);
    }
    console.log(data);

  }
volverprin(){
   this.navCtrl.push("InicioPage");

}
   public infomas (): void{

    var zon_id = "2224946";
    let url: string ='http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"SISTEMA_VALLE-1808","parametros":'{"id_mascota":"198"}'}
    this.httpService.requestResource (url, param).subscribe (
              _data => {
    console.log ("DATOS",_data);
              
                var var_aux: any = _data;
                if (_data != "[{}]") {
                  for (let i = 0; i < _data.length; i ++) {
                    // if (var_aux[i].xvia_ae_data) {

                    var_aux[i].nombre_mas = (var_aux[i].nombre_mas);
                     var_aux[i].foto = (var_aux[i].foto);


                    // var_aux[i].xzona_data = JSON.parse (var_aux[i].xzona_data);
                    // }
                  }
                  this.holala=JSON.stringify(var_aux[0].nombre_mas);
                  this.picture=JSON.stringify(var_aux[0].foto);
                  console.log('epifanio',this.holala);
                  this.data_listar = var_aux;
          
                  console.log('comboooo via',this.data_listar);
                }

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });
    console.log('suer',this.holala);
  }

   public informacionmas (): void{

    var zon_id = "2224946";
       let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CEMENTERIO-5","parametros":'{"id_mascota":"'+this.id_mas+'"}'}
    this.httpService.requestResource (url, param).subscribe (
              _data => {
    console.log ("DATOS",_data);
              
               var aux_array: any = [];
                      if (_data != "[{ }]") {
                  for (let i = 0; i < _data.length; i ++) {

                        
                                let var_aux1 = {
                    xmascota_id:_data[i].xmascota_id,
                    nombre_mas: (JSON.parse(_data[i].xmascota_data)).nombre_mas,
                    especie: (JSON.parse(_data[i].xmascota_data)).especie,
                    raza: (JSON.parse(_data[i].xmascota_data)).raza,
                    sexo: (JSON.parse(_data[i].xmascota_data)).sexo,
                      peso: (JSON.parse(_data[i].xmascota_data)).peso,
                      edad: (JSON.parse(_data[i].xmascota_data)).edad,
                        tamanio: (JSON.parse(_data[i].xmascota_data)).tamanio,
                          color: (JSON.parse(_data[i].xmascota_data)).color,
                            marca: (JSON.parse(_data[i].xmascota_data)).marca,
                             vete_actual: (JSON.parse(_data[i].xmascota_data)).vete_actual,
                 foto:"data:image/jpeg;base64,"+(JSON.parse(_data[i].xmascota_data)).foto
                 
                  }


                  //console.log("scscs11:",(JSON.parse(_data[i].xmascota_data)).foto);
                  // this.imgsdata = 'data:image/png;base64,'+ (JSON.parse(_data[0].xmascota_data)).foto.toString();
                  //data:image/png;base64,
                 // console.log("scscs11:", this.imgsdata );

         
                  //document.getElementById('imgMascota')
                  //document.getElementById("imgMascota").src = "data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";

                  aux_array.push (var_aux1);

                  //  var_aux[i].xmascota_data = JSON.parse(var_aux[i].xmascota_data);
                  }
                  this.data_listar = (aux_array);
                  
                  console.log('comboooo via',this.data_listar);
                }

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });
    console.log('suer',this.holala);
  }
    listamascota(){
       this.navCtrl.push("MascotaPage");
  }

}
