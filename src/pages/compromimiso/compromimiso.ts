import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';
import { AppSettings } from '../../config/app.settings';
import { HttpProvider } from '../../providers/http/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the CompromimisoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-compromimiso',
  templateUrl: 'compromimiso.html',
})
export class CompromimisoPage {

base64Image:string;
    nombre: string; 
    especie: string; 
    raza: string; 
    sexo: string; 
    peso: string; 
    anios: string; 
    tamano: string; 
    color: string; 
    vacunas: string; 
    marca: string; 
    vete_actual: string; 
    peligro: string; 
     reg_vacunas: string; 
      desparacitacion: string; 
   reg_desparacitacion: string; 
   envio_foto: string; 

 reg_marca: string; 


   private compromiso: string;  
 
    

  constructor(public navCtrl: NavController,private camera:Camera, public navParams: NavParams,private httpService: HttpProvider, public toastCtrl: ToastController) {

    this.nombre=navParams.get('nombre');
    this.especie=navParams.get('especie');
    this.raza=navParams.get('raza');
    this.sexo=navParams.get('sexo');
    this.peso=navParams.get('peso');
    this.anios=navParams.get('anios');
    this.tamano=navParams.get('tamano');
    this.color=navParams.get('color');
    this.vacunas=navParams.get('vacunas');
    this.marca=navParams.get('marca');
    this.vete_actual=navParams.get('vete_actual');
    this.peligro=navParams.get('peligro');
      this.reg_vacunas=navParams.get('reg_vacunas');
      this.reg_marca=navParams.get('reg_marca');
      this.desparacitacion=navParams.get('desparacitacion');
this.reg_desparacitacion=navParams.get('reg_desparacitacion');



    var lol = this.nombre+"  bolvia "+this.vete_actual;
    console.log("hola",lol);
     console.log("hola",this.peso);
      console.log("hola",this.vacunas);
         console.log("hola",this.marca);
         console.log("hola",this.reg_vacunas);
         console.log("hola",this.reg_marca);
         console.log("este lugar es de la desparacitacion",this.desparacitacion);


  }


        volverhyInicio() {
   this.navCtrl.push("InicioPage");
   //  this.navCtrl.push(InicioPage);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CompromimisoPage');
  }

  public registrarFinal () {
    if (this.nombre===undefined) {
      this.showToastDos();
      this.volverhyInicio();
      // code...
    }else{
          this.registrarContactoDos();
    }


    

  }

 openCamera(){

    const options: CameraOptions = {
  quality: 10,
  destinationType: this.camera.DestinationType.DATA_URL,
  encodingType: this.camera.EncodingType.PNG,
  mediaType: this.camera.MediaType.PICTURE
}

this.camera.getPicture(options).then((imageData) => {
 // imageData is either a base64 encoded string or a file URI
 // If it's base64 (DATA_URL):

 this.base64Image = 'data:image/png;base64,' + imageData;
 //this.base64Image =imageData;
   this.envio_foto=imageData;

}, (err) => {
 // Handle error
});
  }
 public registrarContactoDos () {
var fakepicture=this.envio_foto;

     console.log(this.compromiso);
      if (this.compromiso===undefined) {
        this.showToastWithCloseButton();

      }else{

              var dato1 = '[{"nombre":"luis","data":"hola"}]';
      var data2 = '{"marca":"ko","datos":'+dato1+'}';

      if (this.reg_vacunas === undefined) {
        this.reg_vacunas='[{}]';
      }
      if (this.reg_marca ===undefined) {
        this.reg_marca='{}';
      }
      if (this.peligro ===undefined) {
        this.peligro='NO';

      }
        if (this.reg_desparacitacion ===undefined) {
        this.reg_desparacitacion='{}';
      }

      console.log("registro de marca",this.reg_marca);
      console.log("registro de vacunas",this.reg_vacunas);

//    var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion+',"foto":""}';
 // var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion
 
   // con amarilllo var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion+',"foto":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhAQFRUVFRUQEBcVFQ8QFRUQFRUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0fHx8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMkA+wMBEQACEQEDEQH/xAAYAAEBAQEBAAAAAAAAAAAAAAABAAIDBv/EACcQAAICAgICAgICAwEAAAAAAAABAhEhMQNBEmFRgXHwkdEysfHB/8QAGgEBAQEBAQEBAAAAAAAAAAAAAQACAwQHBv/EACIRAQEBAQEAAwEBAAMBAQAAAAABEQIhEjFBA2EiMnFR8P/aAAwDAQACEQMRAD8A96fKfdfofUKRJEgRRZaCSAkoyESKKimKXYXfxXfwwiuwtv4Ot/FFLsbLmxW1loiUSVEjFK86L3BdzxNK/X/gbcXuCSV410M3PVNz0CUmNli1TS6CW/q539DGGb+giSERJEjKg520RSqim76ppaVewm6JusGmzELv4K1BK8vAXc8FtzwTSvGhm56udz1kWiiFUkr+SluCbnqIloEiSJGgDUkun+Qlv6zLf0S9fZTf0zQOpAmmsBLdErJppqlW8h7rPusi0aVew90e6klW89Ft0W3WfFGraUiKoiBSJJEkiVa4Yp7/AKM9Wz6Z7t/GZ7ZqezaZ9AmkSbcVSd57RmW7jEt+WBpVvI7dO3TWA26t9YNE0SBFuMVWzNt1i26BKIlAGoRT2wu/jPVs+gkb75vMlWpIxpDJFINSJIkmKSRarU1n0Uvil8ais0FvgtuLljWi4tv2uba50baRJEQKSRvni9C3Hbn4klaOHHVtyscdW31nkgksDz1bTzbaeTjSSaecFz1bco56tuVyaNb62LFpMYmuJXsz1cnjPVwcip0XPsXN2KCGm3IZRzV2G+aJdiotLUFbSM25GbcjXNCtBx1aOOrXNG2q6ThWnYfzl7uMTrz1SVHXZxL/APRt6EkcPlertaiaCVStzgkk72Eu0Tq2sUOnW5QVWE6u4J1dwvjXjdl8ruCdXcYrF2aa31kiaJCi1Kh1aCJotSjEr1gtdOHhTWb2dLzectc+/wCllyOfjlj/AE/rb9NT6ZUTG+N63w8abdme+rJ4z31Ylxq2vgr1clV6uSjwy0Xyuad8YksmpTKmi0lxxdr4rsZ1lwb7i8cXa/Ab7i33AJdI8eLMXr3GL17gUcFevTpjEL17itZNFuELvJZbWOusUUduu+ePJ6z7RFHn6raZExQUWpIrVrUI2FotweOS1b41xwt0V6yaOusg5YU6NTrYubs1kGlGNuit8VuQ8kKdFLsHPWzWGLSaHVqSKqt8kEjt/Lz3pznXycfIu+tdZziMFNBE1KNBzdEus0JLRShkSYRt0VuC3Jp8c19BvmrfNDXRb5p3xUO+LW4xt0Zt81m3JpnCnVhOtmjm7ARVG/5ydX0W+G/g6d9TmZyMv6mjz6dalCgl0S6ZxoJdE61SjRS6ZdMuOldhLonW3GBadJceLCde4xOtuM+OLLSnEtWrwxZb6t9Zo1p1tceLsPl7g+WXHOjTWiiJo9M+PM2s7akr7OPfe0/9WaDWtCQ2q1qELM9dYL1jLRrfDqjG3RW5FbkSVuityarcmssZ6YaJAkiLaji/oN9xnfcZRJ2jx2rs53rLjF6y4yo4s1rVuXAkGpuELC3Gb1jJFqMbC3BbgZFqEb7C3GbcSRatFEdTJRIlSl0Q1SJT1lodKEokGJDRKJoZUZRopdEutc3D49hz38hx38nNxo1LrUussWo1OFK7CdbcZnW3GKNNIiUiCJKKsrcitxrxzXeg2ZrOzNPhmvoN2at2aad1ZT42bWdmanCjPy0y6XGgl1S6qL7X21yQoJdZ56lTjQbql1lDWiALg19jsrM624XClZndUu+JxxZaZfwCVZINkjRatZYpUaufhQIUK0MZCCLok5d/+mbZyxc5ZjC7zo1epDesC47bXwPyk+1esms09WOyemfWpRzRb5rVvmhjFFRJV2G+4t9xIk14vYb+DZ9Guy38G/jcOJ15X+oxepLjN6m4KbyWyeHZLijFvJWyC2TwqNlbitwNlhw2AMYhqtxUQaSbDcFsiSeiXk9VdEtiULdFqvWCUadFpl0JCrVJVgp6pdFCVJEoBKaJMsSZwa2UujnqX6Ck0Nhs1NtdlMq8otrsfKclU4PdlOpfBLL4yaaBFIg1GLeAtkmi2T1pQd0XPovXmnx6OlnPPN37Z3fRXRx39a/1pSer/WZuX1myfaprBbL6fL6sovKvKkyFalCthLql0FhSYUUkGqaDweVIkXF7DZ9DZ9JXsV59Bp7IzPoUduf57Not/AzHdm/8WoVB1ZnVepuKMG1ZWxXqS4z4jp1k0TGNhbi+UiSbLZFsjBstRg2ZvUgvUgUHlfA/KT0/IqLeL0WyejZPWHxu67NTqZrXymayx8MIefSbjBrP9DOZ34xep9NO9nXvrnj/AIyeszL4lF/5HnvU3Ds/6s32Xn01/jSi9/BnZ9C2fRSbyXk8GyeAi1GDYWyC2RNtj8R5AkBajG9BaLcUY2Gi1rOiXk9AJq3oh59hjJq8+yj0c8TmW9C3fplnLr+nyakDMFKT0WCyfZtrHyXn2sl9Zkmh8pllZaNNJNrQ5osilFx9WHlUs6YNNtRm1orzL9iyUyTWcZCZfGZZfGfJrPyOS+NZL4JX/l89jM+lPj9MM001GLeMm5x/98ZvUnrbm/8AH9wPXXPP/VmczfkFejjc+615PWvJpUZyX0ZL6yLTSm0qDJus5LdUZPQWRWT7NUC8pTaxoMlZsnQQ04gDUW0FVkpiysFhyC8qJFGpzb9QI7TmcTb9s/YZy67vVakgMkMiBKciWKUmykUkjLZqExg3pFbILZPsvyl7r8IPOV/x5cjcbajBvRXqT7F6k+1KTeKKST0SSesNvRrJGpJ9ryeiyfayfYo3Obfw62ptZWPpG+/6/LyMfGXys29nHJ9NefRUnsMmYLPxNt5LJPFmeKyLVdmWdn0kRrTth5GfJ4XJsMxSSBASTJSZLyGIVG7AfSINRZ14/p8ILAYvVv2ZkD+DK/0MSL6JeBi0CSFBxa2OqWVqM2tBZKLzKlJx+yydGydOTRuVqNxk49BZz0zZOmVNp2ayWY1ksxli0E6yb5sl9Fm+NOf7k6X+1/BOB5OqOGSXTkl1eTqiyfaybqokQSok0pOqM5t1nNugi3GTWDNkrNkqTKmpMB9lMk1FtB9s2SpEimWLNSZApgsVksFkcTFM32SDYmTA2JV5slhnNspJBJIyP00ZScuv4CSQSTlhs1JI19GfI3uinOCcyMWbxr6DYyFX0Xn2vJ6CLUZVkLN8Zsl8Sm7vsrzMxXmWY15O/KjOTMGTMTlmyzzFnmJvsv8AF/gaspcBTClpsAkSpbMiQkmpTsMZkkFkjY75iw2ZGJkQSQleXRYM/RZEJXgfpaHCht1Sygo0U6LNGak6HNX2oSa6KyVWSsWazWgxITNKzRZX1YYSadoLJZislmBu8jPDPI0+RtKOKRmcyXWZzJdZSNa1rp54qjHx91j4+6wjTTcZOqozZN1mybqysUH+ry+hotOlEGlKjOaLNSZJqMqDNFmstGrzZ9rWoSoxVZqFGLCizT5ZLFgslivskvLNks8wNtu6HM8UkzDGeboLPMV58xck7KTFzzjmbaLnYSKcrknY884pzgnOykwznA3Zvji36X0yyjUAkMp/9Ssc/UCJRBri5KdmeudmM9c7MPJO3Zc85MXPOTGRLpxTp2Y6mzGepsxrk5badaMznJjM5yYzKVjJhkwWWEssBspztyL6abOvPE4m9Mf+Czn338mpMBgpEm2wGCyWFzxRZ6Pj7q88Fiz1mxLSnigz0fH1lSHDgsSYzpNfJWaLzt1hmm0pFYLNUWdOf5XrKOkpUdO+p9cr479sM5NmLoLNVmiMqGzZis1RnTsLzsxWeYJZZqeQyeNy5MUYnOXWZzl1yOss/WijNTVgHSPJiqMXn3Wbz7oUis9WBMiUyFjUWXxtvgqR6M5/n9s+0Hn6627W8x0450Y6msdc6xJ5JqFEkSRJWSBq5+DEZJjKmWKzYpyt2UmDmZGWajQoSBTUnfVHT+X8/wBrH14zKQ9d+ZDzFKRykakPJyXSrRc85o55ysSZqRqRkWm3PFGZz7rE591g001xyp3Vh1Ng6mzCpK7rHwVnmD43MZm7d6GfTUmRJgq3CVOws8xmzzGuSVuzPMyDmZGBaabv0PHG+az9RqT/AB/s6348TJ7WZLTyTvqjz+/tXPOMk06cnImZkxjnmw8nImgkxc82VzsWkSVkjZLPQSQppTVVQZ7rPxu6zeBxrEmOLDGdJqthZ6LzbdUXSPRP5z/taOttPFypbX+jn/Xer59K8WuIOqUqGzRYyzTRi6CzRZqjIOpbFYVLNllzFnjMmakyGQt40GXRl1kWkSIA2QbjJUZsuiy6yJMWHsuwWaiRTM0JiSAUX6LBY1CSW1YWKy36ZbLEiJiyFTZKAiY7L8FnhlNXdY+CkuCS4pyV2lgpLhkuM8sk3hUPMsnq5lk9ZbN7b9tSAi1ySXSM8yz7Z5l/XM6Nt8klSpfkzzLvrHPNn2xYyNtzkqSSz28BJdZksttrlRtsgiQRIESQNggzUmgxM1NMzBAhpdOSSelRib+scyz7ZFpqbXS/ISX9Z5l/VJoJKprFm/j5pbdGfRN0tqljPYZdEl1kWjaoMHugSrJMs1CbKzACIEtRa7M2W/TN1g201BpbCy36HUt+mYtdjZfw3fxmzUmkF9GOnE0t/wBmO5b9Mdy36HI84HnyennyeltUvnsJu/4pusmj7qIr9+iHoskUSK/4A9RJpGaLqRVAi3AzdZosVhjXYXfwXfxRecldNlwxavOiyiy54GJjLKEwfyVF/wAabV+g9wZcHI1eNDNz087nrLGQqykq9a5Wug5l/Rzv6zNro1N/Tzv6yxmmMmvGkSaddb7Mzd9Zm/qlVY32U3fVN10m1XRiTrWJLridHVCm+Krzoz1ueM9bninV410XMtkn6JueqKXZ1vGc7ftW38EqvGjlNz0zc9EvRT/TDD3+or/iv141yVePvZmbnrHO56rRSU+mddBzv6JrItOvL49Gefl+ufO/rM6xX2U39PPy/WRaRJt1WN/YTdZm6X4+Pv7Cbo/5fL/GMUPutelLHs6c/wA71f8ABblZDqSXIY3Gq9mLui7rCNNJUV1et8LjT8t9bDqdb4x38t8cTbqmJSK7+C6BLUKvOjN3PGet/AhKRL0NliLSoJuj1Uaktvi0nf8A48ej2snG9W/bUmNYru+vwY90e6BLUarOzN3Rd0ISzIZidIV2Z6l/GLphXYXfxXfxlEUiqRFEmoVeQu54zd/Ckr9F7n+i7i8Vfo7cfz/4/wDIfK5/odX6Dr+lsyGS5oZzn01AxhCYlEDOugm/qm/rL9Goon6L39MTSCb+qA0WpVSrfZmazN/TJKlW+ym6pusC0iTfEk3nX8Flv/Vju2TxTpNpHf5TjnP0c717RHLz9nHu2+/rV8nimleNGZueqbgYkpZ9B7g/DNK8Bzbnq5tz1lmimyRKWjGppdMzzb+jm39DKGF0U0TTJKlTz2E3RN31kWkSdHFeN3n4wdOObLvX05fLq3GGy7/pb5+NyYLOf60zYltJU856Qe6xbd/xRSp5z0V3Td0KqdvPRXd8V3WULVUf+Dtn0qBt1GJm7+KgS3xpN5dGerZPGOrZPAkr9Ddw+4klforuK7gYwxR1/H+0erj/AK3/ANY6+wea/dbiApDUgSJIkiSRIokgBJAkUSRIx2vydf5fc/8AWOlL9/hD/X8//fkXIZxbAogkSBECkxSBIkkKQJEkSRJEn//Z"}';
  // var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion
 
  // var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion+',"foto":"'+this.envio_foto+'"}';
  //   var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion+',"foto":"'+ki+'"}';

var dataEnvio  ='{"nombre_mas":"'+this.nombre+'","especie":"'+this.especie+'","raza":"'+this.raza+'","sexo":"'+this.sexo+'","peso":"'+this.peso+'","edad":"'+this.anios+'","tamanio":"'+this.tamano+'","color":"'+this.color+'","vacunas":"'+this.vacunas+'","marca":"'+this.marca+'","vete_actual":"'+this.vete_actual+'","comp_peli":"'+this.peligro+'","reg_vacunas":'+this.reg_vacunas+',"reg_marca":'+this.reg_marca+',"compromiso":"'+this.compromiso+'","desparacitacion":"'+this.desparacitacion+'","reg_desparacitacion":'+this.reg_desparacitacion+',"foto":"'+fakepicture+'"}';
    console.log("objeto: ",dataEnvio);  
  //   let url: string ='http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
   let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CASA_MASCOTA-3","parametros":'{"xmascota_raza_id":1,"xmascota_data":'+JSON.stringify(dataEnvio)+',"xmascota_usr_id":1,"cod_chip":"23","titular_id":2}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);

                this.showToast();

              },
              error => {
                console.log ("DATOS");

                this.showToastDos();
                alert(error);
              });



          this.volverhyInicio();

      }


  }

  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Debe llenar todos los campos',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

    showToast() {
    const toast = this.toastCtrl.create({
      message: 'Registro Satisfactorio',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

      showToastDos() {
    const toast = this.toastCtrl.create({
      message: 'Error al registrar',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

}
