import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrarUnoPage } from '../registrar-uno/registrar-uno';
import { InicioPage } from '../inicio/inicio';
import { InformacionPage } from '../informacion/informacion';
import { ActualizarvacunaPage } from '../actualizarvacuna/actualizarvacuna';
import { HttpProvider } from '../../providers/http/http';
import { AppSettings } from '../../config/app.settings';


/**
 * Generated class for the MascotaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mascota',
  templateUrl: 'mascota.html',
})
export class MascotaPage {
 private data_listar: string;
 private imgsdata:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public httpService: HttpProvider) {
  }
      abrirRegistroUno() {
   this.navCtrl.push("RegistrarUnoPage");
   //  this.navCtrl.push(InicioPage);
  }
        volverInicio() {
   this.navCtrl.push("InicioPage");
   //  this.navCtrl.push(InicioPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MascotaPage');
    this.listarMascota();

  }
 public erica(a,b,c,d,e,f,g,h,i,j,k){
   var llegada =a+" "+b+" "+c+d+e+f+g+h+i+j+k;

    console.log("la raza seleccionada",llegada);

 
     this.navCtrl.setRoot("InformacionPage",{id_mas:a});
 
   //  this.navCtrl.push(InicioPage);
  
  }
   public ericados(a,b,c,d,e,f,g,h,i,j,k){
  var llegada =a+" "+b+" "+c+d+e+f+g+h+i+j+k;
    console.log("la raza seleccionada",llegada);
this.navCtrl.setRoot("ActualizarvacunaPage",{id_mas:a,ynom_mas:b,yespecie:c,yraza:d,ysexo:e,ypeso:f,yedad:g,ytamanio:h,ycolor:i,ymarca:j,yvete_actual:k});

   //  this.navCtrl.push(InicioPage);
  
  }
     public luisprueba(){

    

   this.navCtrl.push("InformacionPage");
   //  this.navCtrl.push(InicioPage);
  
  }
       public codigorprueba(){

    

   this.navCtrl.push("ActualizarvacunaPage");
   //  this.navCtrl.push(InicioPage);
  
  }
  public listarMascota (): void{

    var zon_id = "2224946";
    let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CASA_MASCOTA-1","parametros":'{"xcarnet":"2224946"}'}
    this.httpService.requestResource (url, param).subscribe (
              _data => {
                console.log ("DATOS listar mascota ",_data);
                var aux_array: any = [];
                if (_data != "[{}]") {
                  for (let i = 0; i < _data.length; i ++) {
                    
                                let var_aux1 = {
                    xmascota_id:_data[i].xmascota_id,
                    nombre_mas: (JSON.parse(_data[i].xmascota_data)).nombre_mas,
                    especie: (JSON.parse(_data[i].xmascota_data)).especie,
                    raza: (JSON.parse(_data[i].xmascota_data)).raza,
                    sexo: (JSON.parse(_data[i].xmascota_data)).sexo,
                      peso: (JSON.parse(_data[i].xmascota_data)).peso,
                      edad: (JSON.parse(_data[i].xmascota_data)).edad,
                        tamanio: (JSON.parse(_data[i].xmascota_data)).tamanio,
                          color: (JSON.parse(_data[i].xmascota_data)).color,
                            marca: (JSON.parse(_data[i].xmascota_data)).marca,
                             vete_actual: (JSON.parse(_data[i].xmascota_data)).vete_actual,
                 foto:"data:image/jpeg;base64,"+(JSON.parse(_data[i].xmascota_data)).foto
                 
                  }


                  //console.log("scscs11:",(JSON.parse(_data[i].xmascota_data)).foto);
                  // this.imgsdata = 'data:image/png;base64,'+ (JSON.parse(_data[0].xmascota_data)).foto.toString();
                  //data:image/png;base64,
                  //console.log("scscs11:", this.imgsdata );

         
                  //document.getElementById('imgMascota')
                  //document.getElementById("imgMascota").src = "data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";

                  aux_array.push (var_aux1);
                  /* if (_data [i].xmascota_data) {
                    aux_array.push (JSON.parse (_data [i].xmascota_data));
                    }*/
                  //  var_aux[i].xmascota_data = JSON.parse(var_aux[i].xmascota_data);
                  }
                  this.data_listar = (aux_array);
                  console.log('comboooo via',this.data_listar);
                }

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });
  }
      listamascota(){
       this.navCtrl.push("InicioPage");
  }

}
