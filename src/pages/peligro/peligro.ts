import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CompromimisoPage } from '../compromimiso/compromimiso';
import { VacunasPage } from '../vacunas/vacunas';
import { CertificadoPage } from '../certificado/certificado';
import { TatuajePage } from '../tatuaje/tatuaje';
import { ChipPage } from '../chip/chip';
import { DesparacicitacionPage } from '../desparacicitacion/desparacicitacion';


/**
 * Generated class for the PeligroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-peligro',
  templateUrl: 'peligro.html',
})
export class PeligroPage {

    nombre: string; 
    especie: string; 
    raza: string; 
    sexo: string; 
    peso: string; 
    anios: string; 
    tamano: string; 
    color: string; 
    vacunas: string; 
    marca: string; 
    vete_actual: string; 
     desparacitacion: string; 




   private peligro: string;  
 


  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {

  	this.nombre=navParams.get('nombre');
    this.especie=navParams.get('especie');
    this.raza=navParams.get('raza');
    this.sexo=navParams.get('sexo');
    this.peso=navParams.get('peso');
    this.anios=navParams.get('anios');
    this.tamano=navParams.get('tamano');
    this.color=navParams.get('color');
    this.vacunas=navParams.get('vacuna');
    this.marca=navParams.get('marca');
    this.vete_actual=navParams.get('vete_actual');
    this.desparacitacion=navParams.get('desparacitacion');


console.log(this.nombre);
    console.log(this.vacunas);
        console.log(this.peso);
        console.log(this.marca);
         console.log("esta es la desparacitacion en peligro",this.desparacitacion);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PeligroPage');
  }
      showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Debe llenar todos los campos',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }
public registrocon(){
if (this.peligro===undefined) {

      this.showToastWithCloseButton();
  }else{
    this.abrirMascota();
  }

}
          abrirMascota() {

          	console.log("compromiso de peligro",this.peligro);
              console.log("marca",this.marca);
         

          		if (this.vacunas === 'si') {

          					   this.navCtrl.setRoot("VacunasPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 


          		}else{
          			

          			if (this.marca === 'ninguna') {

                      if (this.desparacitacion==='no') {


                       this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 
                      }else{


                       this.navCtrl.setRoot("DesparacicitacionPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 
                      }

          			}
          				
                if (this.marca === 'certificado') {

                       this.navCtrl.setRoot("CertificadoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 
                }
                if (this.marca === 'tatuaje') {

                       this.navCtrl.setRoot("TatuajePage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 
                }
                if (this.marca === 'chip') {

                       this.navCtrl.setRoot("ChipPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 
                }
                if (this.marca === 'undefined') {

                       this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,desparacitacion:this.desparacitacion});
 
                }
          			
          		}
  }

}
