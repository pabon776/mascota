import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PeligroPage } from './peligro';

@NgModule({
  declarations: [
    PeligroPage,
  ],
  imports: [
    IonicPageModule.forChild(PeligroPage),
  ],
})
export class PeligroPageModule {}
