import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RegistrarDosPage } from '../registrar-dos/registrar-dos';
import { TatuajePage } from '../tatuaje/tatuaje';
import { CertificadoPage } from '../certificado/certificado';
import { CompromimisoPage } from '../compromimiso/compromimiso';
import { ChipPage } from '../chip/chip';
import { VacunasPage } from '../vacunas/vacunas';
import { DesparacicitacionPage } from '../desparacicitacion/desparacicitacion';




import { AppSettings } from '../../config/app.settings';
import { HttpProvider } from '../../providers/http/http';

import { PeligroPage } from '../peligro/peligro';



/**
 * Generated class for the RegistrarUnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrar-uno',
  templateUrl: 'registrar-uno.html',
})
export class RegistrarUnoPage {
private nombre: string;
private especie : string;
private raza: string;  
private sexo: string;  
private peso: string;  
private numero: string;  
private anios: string; 
private aniosdos: string;   
private tamano: string;  
private color: string;  
private gender: string;
private desparacitacion: string;  

private vete_actual: string; 
private razaData: string;
private vacuna: string;  

  private kkko: string;  

   
 private v1: string;


private esterilizacion:string;
private data_combo_raza: string;


private ocultar1: boolean = false;



 
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private httpService: HttpProvider,public toastCtrl: ToastController  ) {


    console.log("hola");
  }

    ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrarUnoPage');
    //this.razasMascota();




  }
      abrirRegistrfoDos() {
       
 if (this.esterilizacion==="no") {

             if (this.nombre===undefined || this.especie===undefined || this.raza===undefined || this.sexo===undefined || this.peso===undefined || this.numero===undefined || this.anios===undefined || this.tamano===undefined || this.color===undefined || this.esterilizacion===undefined || this.vete_actual===undefined || this.vacuna===undefined || this.aniosdos===undefined || this.desparacitacion===undefined) {
              console.log("ingresar todos lo datos");
              this.showToastWithCloseButton();



          }else{


               var  v13=this.numero+" "+this.peso;
               console.log(v13);

                var v14=this.anios+" "+this.aniosdos;
                console.log(v14);
                this.gender='ninguna'


               //console.log("esto es la verdadera raza de especiess",this.raza);




              if (this.raza ==='pitbull' || this.raza==='AMERICAN STAFFORDSHIRE TERRIER' || this.raza==='AMERICAN STAFFORDSHIRE BULL TERRIER' || this.raza==='PIT BULL TERRIER' || this.raza==='BULL TERRIER' || this.raza==='BULLMASTIFF' || this.raza==='DOBERMAN' || this.raza==='DOGO ARGENTINO' || this.raza==='DOGO DE BURDEOS' || this.raza==='FILO BRASILEIRO' || this.raza==='ROTTWEILER' || this.raza==='TOSA INU O DOGO ORIENTAL') {

            this.navCtrl.setRoot("PeligroPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacuna:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});

            }else{

              if (this.vacuna === 'si') {

                    this.navCtrl.setRoot("VacunasPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});


              }else{


                console.log("ESTE ES LA VACUNA",this.vacuna);



                if (this.gender === 'ninguna') {

                      if (this.desparacitacion==='no') {

                           this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 

                      }else{

                          this.navCtrl.setRoot("DesparacicitacionPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 

                      }

                     
                }
                  
                if (this.gender === 'certificado') {

                       this.navCtrl.setRoot("CertificadoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }
                if (this.gender === 'tatuaje') {

                       this.navCtrl.setRoot("TatuajePage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }
                if (this.gender === 'chip') {

                       this.navCtrl.setRoot("ChipPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }
                if (this.gender === ' ') {

                       this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacuna:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }




              }


            }
      }


 }else{
          if (this.nombre===undefined || this.esterilizacion===undefined || this.especie===undefined || this.raza===undefined || this.sexo===undefined || this.peso===undefined || this.numero===undefined || this.anios===undefined || this.tamano===undefined || this.color===undefined || this.gender===undefined || this.vete_actual===undefined || this.vacuna===undefined || this.aniosdos===undefined || this.desparacitacion===undefined) {
              console.log("ingresar todos lo datos");
              this.showToastWithCloseButton();



          }else{


               var  v13=this.numero+" "+this.peso;
               console.log(v13);

                var v14=this.anios+" "+this.aniosdos;
                console.log(v14);


               //console.log("esto es la verdadera raza de especiess",this.raza);




              if (this.raza ==='pitbull' || this.raza==='AMERICAN STAFFORDSHIRE TERRIER' || this.raza==='AMERICAN STAFFORDSHIRE BULL TERRIER' || this.raza==='PIT BULL TERRIER' || this.raza==='BULL TERRIER' || this.raza==='BULLMASTIFF' || this.raza==='DOBERMAN' || this.raza==='DOGO ARGENTINO' || this.raza==='DOGO DE BURDEOS' || this.raza==='FILO BRASILEIRO' || this.raza==='ROTTWEILER' || this.raza==='TOSA INU O DOGO ORIENTAL') {

            this.navCtrl.setRoot("PeligroPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacuna:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});

            }else{

              if (this.vacuna === 'si') {

                    this.navCtrl.setRoot("VacunasPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});


              }else{


                console.log("ESTE ES LA VACUNA",this.vacuna);



                if (this.gender === 'ninguna') {

                      if (this.desparacitacion==='no') {

                           this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 

                      }else{

                          this.navCtrl.setRoot("DesparacicitacionPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 

                      }

                     
                }
                  
                if (this.gender === 'certificado') {

                       this.navCtrl.setRoot("CertificadoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }
                if (this.gender === 'tatuaje') {

                       this.navCtrl.setRoot("TatuajePage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }
                if (this.gender === 'chip') {

                       this.navCtrl.setRoot("ChipPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacunas:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }
                if (this.gender === ' ') {

                       this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:v13,anios:v14,tamano:this.tamano,color:this.color,vacuna:this.vacuna,marca:this.gender,vete_actual:this.vete_actual,desparacitacion:this.desparacitacion});
 
                }




              }


            }
      }


  }

   
   //  this.navCtrl.push(InicioPage);
  }



 action (a){
  if (a==="si") {
    this.ocultar1 = true;
  }else{
    this.ocultar1 = false;
  }
  
}
  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Debe llenar todos los campos',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }




  public ayudaPeticionToken (data) {




console.log("JORGICUS",data);
  }
  public getDatosUsuario () {
    let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"SISTEMA_VALLE-931","parametros":'{"xmascota_raza_id":1}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });
  }

public imprimir():void{

  let huj = this.nombre+" "+this.especie+" "+this.raza+" "+this.sexo+" "+this.peso+" "+this.numero+" "+this.anios+" "+this.tamano+" "+this.color+" "+this.gender+" "+this.vete_actual+" "+this.vacuna;
  console.log(huj);

}


public Mostrar (a){

    console.log("esto es lo que me llega", a);
}




  public razasMascota (a) {

    console.log("la especie al ingresar",this.especie);

    if(this.especie===undefined){

      console.log("seleccione la especie");


    }else{


    if(a==="canino" ){

          let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CASA_MASCOTA-2","parametros":'{"_especie_id":1}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);
              
                var var_aux: any = _data;
                if (_data != "[{ }]") {
                  for (let i = 0; i < _data.length; i ++) {
                    // if (var_aux[i].xvia_ae_data) {
                    var_aux[i].xue_nombre = (var_aux[i].xue_nombre);
                    // var_aux[i].xzona_data = JSON.parse (var_aux[i].xzona_data);
                    // }
this.kkko=JSON.stringify(var_aux[1].xue_nombre);
  console.log('comboooo via',this.kkko);
                  }
                  this.kkko=JSON.stringify(var_aux[1].xue_nombre);
                  this.kkko=var_aux[1].xue_nombre;
                  this.data_combo_raza = var_aux;
                  console.log('comboooo via',this.data_combo_raza);
                  console.log('comboooo via',this.kkko);
                }

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });

    }else{

    let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CASA_MASCOTA-2","parametros":'{"_especie_id":2}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);
              
                var var_aux: any = _data;
                if (_data != "[{ }]") {
                  for (let i = 0; i < _data.length; i ++) {
                    // if (var_aux[i].xvia_ae_data) {
                    var_aux[i].xue_nombre = (var_aux[i].xue_nombre);
                    // var_aux[i].xzona_data = JSON.parse (var_aux[i].xzona_data);
                    // }
                  }
                  this.data_combo_raza = var_aux;
                  console.log('comboooo via',this.data_combo_raza);
                }

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });
      
    }


    }






  }

}
