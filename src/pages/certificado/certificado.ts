import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController } from 'ionic-angular';
import { RegistrarDosPage } from '../registrar-dos/registrar-dos';
import { DesparacicitacionPage } from '../desparacicitacion/desparacicitacion';

/**
 * Generated class for the CertificadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-certificado',
  templateUrl: 'certificado.html',
})
export class CertificadoPage {
      nombre: string; 
    especie: string; 
    raza: string; 
    sexo: string; 
    peso: string; 
    anios: string; 
    tamano: string; 
    color: string; 
    vacunas: string; 
    marca: string; 
    vete_actual: string; 
    peligro: string; 
     reg_vacunas: string; 
      desparacitacion: string;


        private fecha_aplicacion: string;  
        private nombre_veterinario_realizacion: string;
        private institucion: string;  
        private codigo_esterilizacion: string;  




  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController ) {

        this.nombre=navParams.get('nombre');
    this.especie=navParams.get('especie');
    this.raza=navParams.get('raza');
    this.sexo=navParams.get('sexo');
    this.peso=navParams.get('peso');
    this.anios=navParams.get('anios');
    this.tamano=navParams.get('tamano');
    this.color=navParams.get('color');
    this.vacunas=navParams.get('vacunas');
    this.marca=navParams.get('marca');
    this.vete_actual=navParams.get('vete_actual');
    this.peligro=navParams.get('peligro');
      this.reg_vacunas=navParams.get('reg_vacunas');
      this.desparacitacion=navParams.get('desparacitacion');

    var lol = this.nombre+"  bolvia "+this.vete_actual;
    console.log("hola",lol);
     console.log("hola",this.peso);
      console.log("hola",this.vacunas);
         console.log("hola",this.marca);
         console.log("hola",this.reg_vacunas);

           console.log("DESPARACITACION",this.desparacitacion);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CertificadoPage');
  }

      abrirRegistroDos() {

        if (this.fecha_aplicacion===undefined || this.nombre_veterinario_realizacion===undefined || this.institucion===undefined ) {

          this.showToastWithCloseButton();

        }else{

        var reg_marca ='{"nombre_marca":"certificacion","fecha_aplicacion":"'+this.fecha_aplicacion+'","nombre_veterinario_realizacion":"'+this.nombre_veterinario_realizacion+'","institucion":"'+this.institucion+'","codigo_esterilizacion":"'+this.codigo_esterilizacion+'"}';
        console.log(reg_marca);

        if (this.desparacitacion==='no') {

            this.navCtrl.setRoot("CompromimisoPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:this.reg_vacunas,reg_marca:reg_marca,desparacitacion:this.desparacitacion});
 
        }else {

             this.navCtrl.setRoot("DesparacicitacionPage",{nombre:this.nombre,especie:this.especie,raza:this.raza,sexo:this.sexo,peso:this.peso,anios:this.anios,tamano:this.tamano,color:this.color,vacunas:this.vacunas,marca:this.marca,vete_actual:this.vete_actual,peligro:this.peligro,reg_vacunas:this.reg_vacunas,reg_marca:reg_marca,desparacitacion:this.desparacitacion});
 
        }

     
       }


  }


    showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Debe llenar todos los campos',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }



}
