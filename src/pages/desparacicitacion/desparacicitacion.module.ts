import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DesparacicitacionPage } from './desparacicitacion';

@NgModule({
  declarations: [
    DesparacicitacionPage,
  ],
  imports: [
    IonicPageModule.forChild(DesparacicitacionPage),
  ],
})
export class DesparacicitacionPageModule {}
