import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MascotaPage } from '../mascota/mascota';

import { AppSettings } from '../../config/app.settings';
import { HttpProvider } from '../../providers/http/http';

/**
 * Generated class for the ActualizarvacunaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-actualizarvacuna',
  templateUrl: 'actualizarvacuna.html',
})
export class ActualizarvacunaPage {
  private id_mas:string;
   private data_listar:string;
      private idd_mas:string;

       private tipo_vacuna1:string;
       private nro_dosis1:string;
       private nomb_vete1:string;
       private inst_vete1:string;
      private enviovac:string;


      private ynom_mas:string;
      private yespecie:string;
      private ysexo:string;
      private ypeso:string;
      private yedad:string;
      private ytamanio:string;
      private ycolor:string;
      private ymarca:string;
      private yvete_actual:string;
      private yraza:string;




  constructor(public navCtrl: NavController, public navParams: NavParams, private httpService: HttpProvider) {
      this.id_mas=navParams.get('id_mas');
      this.ynom_mas=navParams.get('ynom_mas');
      this.yespecie=navParams.get('yespecie');
      this.ysexo=navParams.get('ysexo');
      this.ypeso=navParams.get('ypeso');
      this.yedad=navParams.get('yedad');
      this.ytamanio=navParams.get('ytamanio');
      this.ycolor=navParams.get('ycolor');
      this.ymarca=navParams.get('ymarca');
      this.yvete_actual=navParams.get('yvete_actual');
      this.yraza=navParams.get('yraza');
  



    console.log("el id que llega",this.id_mas);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActualizarvacunaPage');
     console.log("el id que llega",this.id_mas);
   
  }

  public peticionToken (): void {
    // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
    let url: string ="http://172.19.161.3/api/apiLogin";
    let params: any = {"usr_usuario": "administrador", "usr_clave": "123456"};
    this.httpService.loginResource (url, params).
        subscribe (
              _token => this.ayudaPeticionToken (_token),
              error => {
                alert(error);
              });
  }

  public ayudaPeticionToken (data) {
      if ( typeof(data.token) != 'undefined' ) {
      localStorage.setItem ('TOKEN_CAJERO', data.token);
    }
    console.log(data);

  }
     public infomasdos (): void{

  
    let url: string ='http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
   // let param: any = {"identificador":"SISTEMA_VALLE-1809","parametros":'{"xmascotas_id":1,"xmascotas_nombre":"nnn","xmascotas_especie":"nnn","xmascotas_raza":"bnbn","xmascotas_sexo":"nmbn","xmascotas_peso":"nnmnm","xmascotas_edad":5,"xmascotas_tamanio":"grande","xmascotas_color":"negro","xmascotas_marca":"ninguna","xmascotas_veterinario_actual":"nmn","xmascotas_veterinario_actual":"nmn","xmascotas_usr_id":1}'}
    let param: any = {"identificador":"SISTEMA_VALLE-1809","parametros":'{"xmascotas_id":1,"xmascotas_nombre":"nn","xmascotas_especie":"nnn","xmascotas_raza":"bnbn","xmascotas_sexo":"nmbn","xmascotas_peso":"nnmnm","xmascotas_edad":5,"xmascotas_tamanio":"grande","xmascotas_color":"negro","xmascotas_marca":"ninguna","xmascotas_veterinario_actual":"nmn","xmascotas_veterinario_actual":"nmn","xmascotas_usr_id":1,"xmascotas_vacuna":"nmnm"}'};
    this.httpService.requestResource (url, param).
        subscribe (
              _data => {
                console.log ("DATOS",_data);


              },
              error => {
                console.log ("DATOS");

          
                alert(error);
              });
  }
     public informacionmas (): void{

    var zon_id = "2224946";
       let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CEMENTERIO-5","parametros":'{"id_mascota":"'+this.id_mas+'"}'}
    this.httpService.requestResource (url, param).subscribe (
              _data => {
    console.log ("DATOS",_data);
              
               var aux_array: any = [];
                      if (_data != "[{ }]") {
                  for (let i = 0; i < _data.length; i ++) {

                   if (_data [i].xmascota_data) {
                               let var_aux1 = {
                    xmascota_id:_data[i].xmascota_id,
                    nombre_mas: (JSON.parse(_data[i].xmascota_data)).nombre_mas

                  
                  }
             
                  //console.log("scscs",nombre_mas);
                 aux_array.push (var_aux1);
   // console.log("scscs",var_aux1);

                   
                  }
                
                }
              }

              },
              error => {
                console.log ("DATOS");
                alert(error);
              });

  }
       public informacionmasdos (): void{

    var jason = '{"tipo_vacuna":"'+this.tipo_vacuna1+'","nro_dosis":"'+this.nro_dosis1+'","nomb_vete":"'+this.nomb_vete1+'","inst_vete":"'+this.inst_vete1+'"},';


    var zon_id = "2224946";
       let url: string = AppSettings.EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
    let param: any = {"identificador":"CEMENTERIO-6","parametros":'{"xmascotas_id":"'+this.id_mas+'","xmascotas_nombre":"'+this.ynom_mas+'","xmascotas_especie":"'+this.yespecie+'","xmascotas_raza":"'+this.yraza+'","xmascotas_sexo":"'+this.ysexo+'","xmascotas_peso":"'+this.ypeso+'","xmascotas_edad":2,"xmascotas_tamanio":"'+this.ytamanio+'","xmascotas_color":"'+this.ycolor+'","xmascotas_marca":"'+this.ymarca+'","xmascotas_veterinario_actual":"'+this.yvete_actual+'","xmascotas_vacuna":'+JSON.stringify(jason)+',"xmascotas_usr_id":1}'}
    this.httpService.requestResource (url, param).subscribe (
              _data => {
    console.log ("DATOS",_data);
              
this.listamascota();
              },
              error => {
                console.log ("DATOS");
                alert(error);
              });

  }
  listamascota(){
       this.navCtrl.push("MascotaPage");
  }

}
