import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActualizarvacunaPage } from './actualizarvacuna';

@NgModule({
  declarations: [
    ActualizarvacunaPage,
  ],
  imports: [
    IonicPageModule.forChild(ActualizarvacunaPage),
  ],
})
export class ActualizarvacunaPageModule {}
