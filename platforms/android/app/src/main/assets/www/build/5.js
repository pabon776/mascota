webpackJsonp([5],{

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompromimisoPageModule", function() { return CompromimisoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__compromimiso__ = __webpack_require__(692);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CompromimisoPageModule = /** @class */ (function () {
    function CompromimisoPageModule() {
    }
    CompromimisoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__compromimiso__["a" /* CompromimisoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__compromimiso__["a" /* CompromimisoPage */]),
            ],
        })
    ], CompromimisoPageModule);
    return CompromimisoPageModule;
}());

//# sourceMappingURL=compromimiso.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    AppSettings.EP_LOCAL = 'http://127.0.0.1:8000/';
    AppSettings.EP_VALLE = 'http://137.117.66.239/';
    //public static EP_VALLE = 'http://172.19.161.3:5050/';
    AppSettings.IGOB_CIUDADANO_FOTO = 'http://23.96.17.251:27017/';
    return AppSettings;
}());

//# sourceMappingURL=app.settings.js.map

/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompromimisoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_app_settings__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(345);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CompromimisoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CompromimisoPage = /** @class */ (function () {
    function CompromimisoPage(navCtrl, camera, navParams, httpService, toastCtrl) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.navParams = navParams;
        this.httpService = httpService;
        this.toastCtrl = toastCtrl;
        this.nombre = navParams.get('nombre');
        this.especie = navParams.get('especie');
        this.raza = navParams.get('raza');
        this.sexo = navParams.get('sexo');
        this.peso = navParams.get('peso');
        this.anios = navParams.get('anios');
        this.tamano = navParams.get('tamano');
        this.color = navParams.get('color');
        this.vacunas = navParams.get('vacunas');
        this.marca = navParams.get('marca');
        this.vete_actual = navParams.get('vete_actual');
        this.peligro = navParams.get('peligro');
        this.reg_vacunas = navParams.get('reg_vacunas');
        this.reg_marca = navParams.get('reg_marca');
        this.desparacitacion = navParams.get('desparacitacion');
        this.reg_desparacitacion = navParams.get('reg_desparacitacion');
        var lol = this.nombre + "  bolvia " + this.vete_actual;
        console.log("hola", lol);
        console.log("hola", this.peso);
        console.log("hola", this.vacunas);
        console.log("hola", this.marca);
        console.log("hola", this.reg_vacunas);
        console.log("hola", this.reg_marca);
        console.log("este lugar es de la desparacitacion", this.desparacitacion);
    }
    CompromimisoPage.prototype.volverhyInicio = function () {
        this.navCtrl.push("InicioPage");
        //  this.navCtrl.push(InicioPage);
    };
    CompromimisoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompromimisoPage');
    };
    CompromimisoPage.prototype.registrarFinal = function () {
        if (this.nombre === undefined) {
            this.showToastDos();
            this.volverhyInicio();
            // code...
        }
        else {
            this.registrarContactoDos();
        }
    };
    CompromimisoPage.prototype.openCamera = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            _this.base64Image = 'data:image/jpeg;base64,' + imageData;
        }, function (err) {
            // Handle error
        });
    };
    CompromimisoPage.prototype.registrarContactoDos = function () {
        var _this = this;
        console.log(this.compromiso);
        if (this.compromiso === undefined) {
            this.showToastWithCloseButton();
        }
        else {
            var dato1 = '[{"nombre":"luis","data":"hola"}]';
            var data2 = '{"marca":"ko","datos":' + dato1 + '}';
            if (this.reg_vacunas === undefined) {
                this.reg_vacunas = '[{}]';
            }
            if (this.reg_marca === undefined) {
                this.reg_marca = '{}';
            }
            if (this.peligro === undefined) {
                this.peligro = 'NO';
            }
            if (this.reg_desparacitacion === undefined) {
                this.reg_desparacitacion = '{}';
            }
            console.log("registro de marca", this.reg_marca);
            console.log("registro de vacunas", this.reg_vacunas);
            var dataEnvio = '{"nombre_mas":"' + this.nombre + '","especie":"' + this.especie + '","raza":"' + this.raza + '","sexo":"' + this.sexo + '","peso":"' + this.peso + '","edad":"' + this.anios + '","tamanio":"' + this.tamano + '","color":"' + this.color + '","vacunas":"' + this.vacunas + '","marca":"' + this.marca + '","vete_actual":"' + this.vete_actual + '","comp_peli":"' + this.peligro + '","reg_vacunas":' + this.reg_vacunas + ',"reg_marca":' + this.reg_marca + ',"compromiso":"' + this.compromiso + '","desparacitacion":"' + this.desparacitacion + '","reg_desparacitacion":' + this.reg_desparacitacion + '}';
            console.log("objeto: ", dataEnvio);
            //   let url: string ='http://172.19.161.3/api/reglaNegocio/ejecutarWeb';
            var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
            var param = { "identificador": "SERVICIO_VALLE-396", "parametros": '{"xmascota_raza_id":1,"xmascota_data":' + JSON.stringify(dataEnvio) + ',"xmascota_usr_id":1,"cod_chip":"23","titular_id":2}' };
            this.httpService.requestResource(url, param).
                subscribe(function (_data) {
                console.log("DATOS", _data);
                _this.showToast();
            }, function (error) {
                console.log("DATOS");
                _this.showToastDos();
                alert(error);
            });
            this.volverhyInicio();
        }
    };
    CompromimisoPage.prototype.showToastWithCloseButton = function () {
        var toast = this.toastCtrl.create({
            message: 'Debe llenar todos los campos',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    CompromimisoPage.prototype.showToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Registro Satisfactorio',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    CompromimisoPage.prototype.showToastDos = function () {
        var toast = this.toastCtrl.create({
            message: 'Error al registrar',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    CompromimisoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-compromimiso',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/compromimiso/compromimiso.html"*/'<!--\n  Generated template for the CompromimisoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title>Compromimiso</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n   <P>\n     \n   \n        COMPROMISO DE TENENCIA REPONSABLE DE ANIMALES DE COMPAÑIA\n\n     \n </P>\n        \n\n\n      <p>\n        \n       En mi calidad de titular de la mascota registrada ,  me comprometo a no maltratar ni abandonar , brindar el cuidado y atenciona a la mascota bajo mi custodia , asi como el cumplimiento de las normas legales en actual vigencia\n      </p>\n    <!--ion-input type="text" [(ngModel)] =" compromiso " placeholder="Yo XXXXXX titular de XXXXXX me comprometo....."></ion-input-->\n\n<ion-grid>\n    <ion-row radio-group [(ngModel)] = " compromiso " >\n        <ion-col>\n            <ion-item>\n             \n                <ion-label>ACEPTAR</ion-label>\n                <ion-radio value="En mi calidad de titular de la mascota registrada ,  me comprometo a no maltratar ni abandonar , brindar el cuidado y atenciona a la mascota bajo mi custodia , asi como el cumplimiento de las normas legales en actual vigencia" ></ion-radio>\n            </ion-item>\n        </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n<button ion-button full (click)="openCamera()">CAMARA</button>\n<img [src]="base64Image"/>\n<button ion-button block round color="boton" (click) = " registrarFinal() ">\n    FINALIZAR\n  </button>\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/compromimiso/compromimiso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], CompromimisoPage);
    return CompromimisoPage;
}());

//# sourceMappingURL=compromimiso.js.map

/***/ })

});
//# sourceMappingURL=5.js.map