webpackJsonp([0],{

/***/ 687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrarUnoPageModule", function() { return RegistrarUnoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registrar_uno__ = __webpack_require__(702);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegistrarUnoPageModule = /** @class */ (function () {
    function RegistrarUnoPageModule() {
    }
    RegistrarUnoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__registrar_uno__["a" /* RegistrarUnoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registrar_uno__["a" /* RegistrarUnoPage */]),
            ],
        })
    ], RegistrarUnoPageModule);
    return RegistrarUnoPageModule;
}());

//# sourceMappingURL=registrar-uno.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    AppSettings.EP_LOCAL = 'http://127.0.0.1:8000/';
    AppSettings.EP_VALLE = 'http://137.117.66.239/';
    //public static EP_VALLE = 'http://172.19.161.3:5050/';
    AppSettings.IGOB_CIUDADANO_FOTO = 'http://23.96.17.251:27017/';
    return AppSettings;
}());

//# sourceMappingURL=app.settings.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrarUnoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_app_settings__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(344);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RegistrarUnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegistrarUnoPage = /** @class */ (function () {
    function RegistrarUnoPage(navCtrl, navParams, httpService, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpService = httpService;
        this.toastCtrl = toastCtrl;
        this.ocultar1 = false;
        console.log("hola");
    }
    RegistrarUnoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistrarUnoPage');
        //this.razasMascota();
    };
    RegistrarUnoPage.prototype.abrirRegistrfoDos = function () {
        if (this.esterilizacion === "no") {
            if (this.nombre === undefined || this.especie === undefined || this.raza === undefined || this.sexo === undefined || this.peso === undefined || this.numero === undefined || this.anios === undefined || this.tamano === undefined || this.color === undefined || this.esterilizacion === undefined || this.vete_actual === undefined || this.vacuna === undefined || this.aniosdos === undefined || this.desparacitacion === undefined) {
                console.log("ingresar todos lo datos");
                this.showToastWithCloseButton();
            }
            else {
                var v13 = this.numero + " " + this.peso;
                console.log(v13);
                var v14 = this.anios + " " + this.aniosdos;
                console.log(v14);
                this.gender = 'ninguna';
                //console.log("esto es la verdadera raza de especiess",this.raza);
                if (this.raza === 'pitbull' || this.raza === 'AMERICAN STAFFORDSHIRE TERRIER' || this.raza === 'AMERICAN STAFFORDSHIRE BULL TERRIER' || this.raza === 'PIT BULL TERRIER' || this.raza === 'BULL TERRIER' || this.raza === 'BULLMASTIFF' || this.raza === 'DOBERMAN' || this.raza === 'DOGO ARGENTINO' || this.raza === 'DOGO DE BURDEOS' || this.raza === 'FILO BRASILEIRO' || this.raza === 'ROTTWEILER' || this.raza === 'TOSA INU O DOGO ORIENTAL') {
                    this.navCtrl.setRoot("PeligroPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacuna: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                }
                else {
                    if (this.vacuna === 'si') {
                        this.navCtrl.setRoot("VacunasPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                    }
                    else {
                        console.log("ESTE ES LA VACUNA", this.vacuna);
                        if (this.gender === 'ninguna') {
                            if (this.desparacitacion === 'no') {
                                this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                            }
                            else {
                                this.navCtrl.setRoot("DesparacicitacionPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                            }
                        }
                        if (this.gender === 'certificado') {
                            this.navCtrl.setRoot("CertificadoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                        if (this.gender === 'tatuaje') {
                            this.navCtrl.setRoot("TatuajePage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                        if (this.gender === 'chip') {
                            this.navCtrl.setRoot("ChipPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                        if (this.gender === ' ') {
                            this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacuna: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                    }
                }
            }
        }
        else {
            if (this.nombre === undefined || this.esterilizacion === undefined || this.especie === undefined || this.raza === undefined || this.sexo === undefined || this.peso === undefined || this.numero === undefined || this.anios === undefined || this.tamano === undefined || this.color === undefined || this.gender === undefined || this.vete_actual === undefined || this.vacuna === undefined || this.aniosdos === undefined || this.desparacitacion === undefined) {
                console.log("ingresar todos lo datos");
                this.showToastWithCloseButton();
            }
            else {
                var v13 = this.numero + " " + this.peso;
                console.log(v13);
                var v14 = this.anios + " " + this.aniosdos;
                console.log(v14);
                //console.log("esto es la verdadera raza de especiess",this.raza);
                if (this.raza === 'pitbull' || this.raza === 'AMERICAN STAFFORDSHIRE TERRIER' || this.raza === 'AMERICAN STAFFORDSHIRE BULL TERRIER' || this.raza === 'PIT BULL TERRIER' || this.raza === 'BULL TERRIER' || this.raza === 'BULLMASTIFF' || this.raza === 'DOBERMAN' || this.raza === 'DOGO ARGENTINO' || this.raza === 'DOGO DE BURDEOS' || this.raza === 'FILO BRASILEIRO' || this.raza === 'ROTTWEILER' || this.raza === 'TOSA INU O DOGO ORIENTAL') {
                    this.navCtrl.setRoot("PeligroPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacuna: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                }
                else {
                    if (this.vacuna === 'si') {
                        this.navCtrl.setRoot("VacunasPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                    }
                    else {
                        console.log("ESTE ES LA VACUNA", this.vacuna);
                        if (this.gender === 'ninguna') {
                            if (this.desparacitacion === 'no') {
                                this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                            }
                            else {
                                this.navCtrl.setRoot("DesparacicitacionPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                            }
                        }
                        if (this.gender === 'certificado') {
                            this.navCtrl.setRoot("CertificadoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                        if (this.gender === 'tatuaje') {
                            this.navCtrl.setRoot("TatuajePage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                        if (this.gender === 'chip') {
                            this.navCtrl.setRoot("ChipPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacunas: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                        if (this.gender === ' ') {
                            this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: v13, anios: v14, tamano: this.tamano, color: this.color, vacuna: this.vacuna, marca: this.gender, vete_actual: this.vete_actual, desparacitacion: this.desparacitacion });
                        }
                    }
                }
            }
        }
        //  this.navCtrl.push(InicioPage);
    };
    RegistrarUnoPage.prototype.action = function (a) {
        if (a === "si") {
            this.ocultar1 = true;
        }
        else {
            this.ocultar1 = false;
        }
    };
    RegistrarUnoPage.prototype.showToastWithCloseButton = function () {
        var toast = this.toastCtrl.create({
            message: 'Debe llenar todos los campos',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    RegistrarUnoPage.prototype.ayudaPeticionToken = function (data) {
        console.log("JORGICUS", data);
    };
    RegistrarUnoPage.prototype.getDatosUsuario = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
        var param = { "identificador": "SISTEMA_VALLE-931", "parametros": '{"xmascota_raza_id":1}' };
        this.httpService.requestResource(url, param).
            subscribe(function (_data) {
            console.log("DATOS", _data);
        }, function (error) {
            console.log("DATOS");
            alert(error);
        });
    };
    RegistrarUnoPage.prototype.imprimir = function () {
        var huj = this.nombre + " " + this.especie + " " + this.raza + " " + this.sexo + " " + this.peso + " " + this.numero + " " + this.anios + " " + this.tamano + " " + this.color + " " + this.gender + " " + this.vete_actual + " " + this.vacuna;
        console.log(huj);
    };
    RegistrarUnoPage.prototype.Mostrar = function (a) {
        console.log("esto es lo que me llega", a);
    };
    RegistrarUnoPage.prototype.razasMascota = function (a) {
        var _this = this;
        console.log("la especie al ingresar", this.especie);
        if (this.especie === undefined) {
            console.log("seleccione la especie");
        }
        else {
            if (a === "canino") {
                var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
                var param = { "identificador": "CASA_MASCOTA-2", "parametros": '{"_especie_id":1}' };
                this.httpService.requestResource(url, param).
                    subscribe(function (_data) {
                    console.log("DATOS", _data);
                    var var_aux = _data;
                    if (_data != "[{ }]") {
                        for (var i = 0; i < _data.length; i++) {
                            // if (var_aux[i].xvia_ae_data) {
                            var_aux[i].xue_nombre = (var_aux[i].xue_nombre);
                            // var_aux[i].xzona_data = JSON.parse (var_aux[i].xzona_data);
                            // }
                        }
                        _this.data_combo_raza = var_aux;
                        console.log('comboooo via', _this.data_combo_raza);
                    }
                }, function (error) {
                    console.log("DATOS");
                    alert(error);
                });
            }
            else {
                var url = __WEBPACK_IMPORTED_MODULE_2__config_app_settings__["a" /* AppSettings */].EP_VALLE + 'api/reglaNegocio/ejecutarWeb';
                var param = { "identificador": "CASA_MASCOTA-2", "parametros": '{"_especie_id":2}' };
                this.httpService.requestResource(url, param).
                    subscribe(function (_data) {
                    console.log("DATOS", _data);
                    var var_aux = _data;
                    if (_data != "[{ }]") {
                        for (var i = 0; i < _data.length; i++) {
                            // if (var_aux[i].xvia_ae_data) {
                            var_aux[i].xue_nombre = (var_aux[i].xue_nombre);
                            // var_aux[i].xzona_data = JSON.parse (var_aux[i].xzona_data);
                            // }
                        }
                        _this.data_combo_raza = var_aux;
                        console.log('comboooo via', _this.data_combo_raza);
                    }
                }, function (error) {
                    console.log("DATOS");
                    alert(error);
                });
            }
        }
    };
    RegistrarUnoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registrar-uno',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/registrar-uno/registrar-uno.html"*/'<!--\n  Generated template for the RegistrarUnoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title >REGISTRAR MASCOTA</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding >\n\n<ion-list>\n<ion-item>\n\n\n      <ion-label stacked>\n         NOMBRE (mascota) :\n      </ion-label>\n \n      <ion-input type="text" [(ngModel)] =" nombre "></ion-input>\n  \n \n</ion-item>\n<ion-item>\n   \n\n   \n       <ion-label >\n     ESPECIE :\n      </ion-label>\n\n    <ion-select  [(ngModel)] ="especie" (ionChange) = "razasMascota ($event)">\n      <ion-option value="canino">CANINO</ion-option>\n      <ion-option value="felino">FELINO</ion-option>\n\n    </ion-select>\n\n   </ion-item>\n\n\n\n   <ion-item>\n\n         <ion-label>\n     RAZA :\n      </ion-label>\n\n    <ion-select  [(ngModel)] =" raza " >\n      <ion-option value="{{var_data._raza_data}}" *ngFor="let var_data of data_combo_raza">{{var_data._raza_data}}</ion-option>\n    </ion-select>\n    \n</ion-item>\n\n\n<ion-item>\n    <ion-label>SEXO :</ion-label>\n    <ion-select [(ngModel)] =" sexo " >\n      <ion-option value="macho" >MACHO</ion-option>\n      <ion-option value="hembra">HEMBRA</ion-option>\n\n    </ion-select>\n</ion-item>\n<ion-item>\n   <ion-label>PESO :</ion-label>\n</ion-item>\n<ion-item>\n<ion-input type="text" color="primary" color="dark" round [(ngModel)] =" numero "></ion-input>\n    <ion-select  [(ngModel)] =" peso ">\n      <ion-option value="kilos">KILOS</ion-option>\n      <ion-option value="gramos">GRAMOS</ion-option>\n\n    </ion-select>\n</ion-item>\n\n\n<ion-item>\n   <ion-label>EDAD :</ion-label>\n</ion-item>\n\n<ion-item>\n      <ion-input type="text"  round [(ngModel)] =" anios "></ion-input>\n\n    <ion-select  [(ngModel)] =" aniosdos ">\n      <ion-option value="años">AÑOS</ion-option>\n      <ion-option value="meses">MESES</ion-option>\n\n    </ion-select>\n</ion-item>\n\n<ion-item>\n  <ion-label>\n        TAMAÑO :\n      </ion-label>\n    <ion-select  [(ngModel)] =" tamano ">\n      <ion-option value="gigante">GIGANTE</ion-option>\n      <ion-option value="grande">GRANDE</ion-option>\n       <ion-option value="mediano" >MEDIANO</ion-option>\n        <ion-option value="pequeno">PEQUEÑO</ion-option>\n    </ion-select>\n</ion-item>\n\n\n<ion-item>\n\n\n      <ion-label stacked>\n         COLOR :\n      </ion-label>\n \n      <ion-input type="text" [(ngModel)] =" color "></ion-input>\n  \n \n</ion-item>\n\n\n<ion-item>\n    <ion-label>\n        VACUNAS :\n      </ion-label>\n\n    <ion-select  selected="true" [(ngModel)] =" vacuna ">\n      <ion-option value="no">NO</ion-option>\n      <ion-option value="si" >SI</ion-option>\n\n    </ion-select>\n</ion-item>\n\n\n\n<ion-item>\n      <ion-label>\n        ESTERILIZACION :\n      </ion-label>\n\n    <ion-select  selected="true" [(ngModel)] =" esterilizacion " (ionChange) = "action ($event)">\n      <ion-option value="no">NO</ion-option>\n      <ion-option value="si" >SI</ion-option>\n\n    </ion-select>\n</ion-item>\n\n<ion-item  *ngIf="ocultar1">\n\n    <ion-label>\n       TIPO DE ESTERILIZACION :\n      </ion-label>\n\n    <ion-select  [(ngModel)] =" gender ">\n      <!--ion-option value="ninguna">NINGUNA</ion-option-->\n      <ion-option value="chip">NINGUNA</ion-option>\n      <ion-option value="certificado">CERTIFICADO</ion-option>\n       <ion-option value="tatuaje">TATUAJE</ion-option>\n        <!--ion-option value="chip" >CHIP</ion-option-->\n    </ion-select>\n</ion-item>\n\n\n<ion-item>\n      <ion-label>\n        DESPARACITACION :\n      </ion-label>\n\n    <ion-select  selected="true" [(ngModel)] =" desparacitacion ">\n      <ion-option value="no">NO</ion-option>\n      <ion-option value="si" >SI</ion-option>\n\n    </ion-select>\n</ion-item>\n\n<ion-item>\n\n\n      <ion-label stacked>\n        NOMBRE DEL VETERINARIO (Actual) :\n      </ion-label>\n \n      <ion-input type="text" [(ngModel)] =" vete_actual "></ion-input>\n  \n \n</ion-item>\n\n<br>\n</ion-list>\n\n\n\n<button ion-button block round color="boton" (click) = " abrirRegistrfoDos() ">\n    CONTINUAR\n  </button>\n\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/registrar-uno/registrar-uno.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], RegistrarUnoPage);
    return RegistrarUnoPage;
}());

//# sourceMappingURL=registrar-uno.js.map

/***/ })

});
//# sourceMappingURL=0.js.map