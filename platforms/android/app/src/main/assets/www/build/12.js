webpackJsonp([12],{

/***/ 675:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CertificadoPageModule", function() { return CertificadoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__certificado__ = __webpack_require__(690);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CertificadoPageModule = /** @class */ (function () {
    function CertificadoPageModule() {
    }
    CertificadoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__certificado__["a" /* CertificadoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__certificado__["a" /* CertificadoPage */]),
            ],
        })
    ], CertificadoPageModule);
    return CertificadoPageModule;
}());

//# sourceMappingURL=certificado.module.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CertificadoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CertificadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CertificadoPage = /** @class */ (function () {
    function CertificadoPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.nombre = navParams.get('nombre');
        this.especie = navParams.get('especie');
        this.raza = navParams.get('raza');
        this.sexo = navParams.get('sexo');
        this.peso = navParams.get('peso');
        this.anios = navParams.get('anios');
        this.tamano = navParams.get('tamano');
        this.color = navParams.get('color');
        this.vacunas = navParams.get('vacunas');
        this.marca = navParams.get('marca');
        this.vete_actual = navParams.get('vete_actual');
        this.peligro = navParams.get('peligro');
        this.reg_vacunas = navParams.get('reg_vacunas');
        this.desparacitacion = navParams.get('desparacitacion');
        var lol = this.nombre + "  bolvia " + this.vete_actual;
        console.log("hola", lol);
        console.log("hola", this.peso);
        console.log("hola", this.vacunas);
        console.log("hola", this.marca);
        console.log("hola", this.reg_vacunas);
        console.log("DESPARACITACION", this.desparacitacion);
    }
    CertificadoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CertificadoPage');
    };
    CertificadoPage.prototype.abrirRegistroDos = function () {
        if (this.fecha_aplicacion === undefined || this.nombre_veterinario_realizacion === undefined || this.institucion === undefined) {
            this.showToastWithCloseButton();
        }
        else {
            var reg_marca = '{"nombre_marca":"certificacion","fecha_aplicacion":"' + this.fecha_aplicacion + '","nombre_veterinario_realizacion":"' + this.nombre_veterinario_realizacion + '","institucion":"' + this.institucion + '","codigo_esterilizacion":"' + this.codigo_esterilizacion + '"}';
            console.log(reg_marca);
            if (this.desparacitacion === 'no') {
                this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: this.reg_vacunas, reg_marca: reg_marca, desparacitacion: this.desparacitacion });
            }
            else {
                this.navCtrl.setRoot("DesparacicitacionPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, reg_vacunas: this.reg_vacunas, reg_marca: reg_marca, desparacitacion: this.desparacitacion });
            }
        }
    };
    CertificadoPage.prototype.showToastWithCloseButton = function () {
        var toast = this.toastCtrl.create({
            message: 'Debe llenar todos los campos',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    CertificadoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-certificado',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/certificado/certificado.html"*/'<!--\n  Generated template for the CertificadoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title >REGISTRAR CERTIFICADO</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-list>\n\n  <ion-item>\n    <ion-label stacked>FECHA DE APLICACION :</ion-label>\n    <ion-input type="text" color="edit" round [(ngModel)] =" fecha_aplicacion "></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>NOMBRE DEL VETERINARIO :</ion-label>\n  <ion-input type="text" color="edit" round [(ngModel)] =" nombre_veterinario_realizacion "></ion-input>\n  </ion-item>\n\n    <ion-item>\n    <ion-label stacked>INSTITUCION  :</ion-label>\n      <ion-input type="text" color="edit" round [(ngModel)] =" institucion "></ion-input>\n  </ion-item>\n\n    <ion-item>\n    <ion-label stacked>OPCIONAL:</ion-label>\n<ion-input type="text" color="edit" round [(ngModel)] =" codigo_esterilizacion "></ion-input>\n  </ion-item>\n\n</ion-list>\n\n\n\n\n      <div padding>\n    <button ion-button  block round color="boton" (click)=" abrirRegistroDos() "  >\n     <ion-icon name="paw" item-start>  REGISTRAR </ion-icon>  </button>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/certificado/certificado.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], CertificadoPage);
    return CertificadoPage;
}());

//# sourceMappingURL=certificado.js.map

/***/ })

});
//# sourceMappingURL=12.js.map