webpackJsonp([9],{

/***/ 683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeligroPageModule", function() { return PeligroPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__peligro__ = __webpack_require__(698);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PeligroPageModule = /** @class */ (function () {
    function PeligroPageModule() {
    }
    PeligroPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__peligro__["a" /* PeligroPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__peligro__["a" /* PeligroPage */]),
            ],
        })
    ], PeligroPageModule);
    return PeligroPageModule;
}());

//# sourceMappingURL=peligro.module.js.map

/***/ }),

/***/ 698:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeligroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PeligroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PeligroPage = /** @class */ (function () {
    function PeligroPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.nombre = navParams.get('nombre');
        this.especie = navParams.get('especie');
        this.raza = navParams.get('raza');
        this.sexo = navParams.get('sexo');
        this.peso = navParams.get('peso');
        this.anios = navParams.get('anios');
        this.tamano = navParams.get('tamano');
        this.color = navParams.get('color');
        this.vacunas = navParams.get('vacuna');
        this.marca = navParams.get('marca');
        this.vete_actual = navParams.get('vete_actual');
        this.desparacitacion = navParams.get('desparacitacion');
        console.log(this.nombre);
        console.log(this.vacunas);
        console.log(this.peso);
        console.log(this.marca);
        console.log("esta es la desparacitacion en peligro", this.desparacitacion);
    }
    PeligroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PeligroPage');
    };
    PeligroPage.prototype.showToastWithCloseButton = function () {
        var toast = this.toastCtrl.create({
            message: 'Debe llenar todos los campos',
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    PeligroPage.prototype.registrocon = function () {
        if (this.peligro === undefined) {
            this.showToastWithCloseButton();
        }
        else {
            this.abrirMascota();
        }
    };
    PeligroPage.prototype.abrirMascota = function () {
        console.log("compromiso de peligro", this.peligro);
        console.log("marca", this.marca);
        if (this.vacunas === 'si') {
            this.navCtrl.setRoot("VacunasPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
        }
        else {
            if (this.marca === 'ninguna') {
                if (this.desparacitacion === 'no') {
                    this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
                }
                else {
                    this.navCtrl.setRoot("DesparacicitacionPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
                }
            }
            if (this.marca === 'certificado') {
                this.navCtrl.setRoot("CertificadoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
            }
            if (this.marca === 'tatuaje') {
                this.navCtrl.setRoot("TatuajePage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
            }
            if (this.marca === 'chip') {
                this.navCtrl.setRoot("ChipPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
            }
            if (this.marca === 'undefined') {
                this.navCtrl.setRoot("CompromimisoPage", { nombre: this.nombre, especie: this.especie, raza: this.raza, sexo: this.sexo, peso: this.peso, anios: this.anios, tamano: this.tamano, color: this.color, vacunas: this.vacunas, marca: this.marca, vete_actual: this.vete_actual, peligro: this.peligro, desparacitacion: this.desparacitacion });
            }
        }
    };
    PeligroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-peligro',template:/*ion-inline-start:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/peligro/peligro.html"*/'<!--\n  Generated template for the PeligroPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar full color="rojo">\n    <ion-title>Peligro</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<p>\n  \n  COMPROMISO TENENCIA RESPONSABLE DE ANIMALES POTENCIALMENTE PELIGROSAS\n\n</p>\n\n      <p>\n        \n       En mi calidad de titular de la mascota registrada ,  hacerme responsable asi como el cumplimiento de las normas legales en actual vigencia\n      </p>\n    <!--ion-input type="text" [(ngModel)] =" compromiso " placeholder="Yo XXXXXX titular de XXXXXX me comprometo....."></ion-input-->\n\n<ion-grid>\n    <ion-row radio-group [(ngModel)] = " peligro " >\n        <ion-col>\n            <ion-item>\n             \n                <ion-label>ACEPTAR</ion-label>\n                <ion-radio value="En mi calidad de titular de la mascota registrada ,  hacerme responsable asi como el cumplimiento de las normas legales en actual vigencia" ></ion-radio>\n            </ion-item>\n        </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n\n\n<button ion-button block round color="boton" (click) = " registrocon() ">\n    ACEPTAR COMPROMISO\n  </button>\n\n</ion-content>\n'/*ion-inline-end:"/home/pabon/Escritorio/proyectos_ultimos/mascota/src/pages/peligro/peligro.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], PeligroPage);
    return PeligroPage;
}());

//# sourceMappingURL=peligro.js.map

/***/ })

});
//# sourceMappingURL=9.js.map