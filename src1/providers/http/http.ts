import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';


import { AppSettings } from '../../config/app.settings';

import { HttpErrorResponse } from '@angular/common/http';

import { Headers, RequestOptions, Response } from '@angular/http';

// NOTICE: Delete map and MessageProvider, maybe use retry.

// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
 
@Injectable()
export class HttpProvider {

    constructor ( public http: HttpClient ) {
    }

    public loginResource (url: string, param: any): Observable<any> {
      console.log ("");
      return this.http.post (url, param )
        .pipe (
            tap((data: any) => data),
            catchError (this.handleError)
        );
    }

    public requestResource (url: string, param: any): Observable<any> {
      return this.http.post (url, param, this.jwt ())
        .pipe(
            tap ((data: any) => data),
            catchError (this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('Ocurrió un error:', error.error.message);
        return Observable.throw (`Error al procesar la solicitud (client-netework error) ${error}`);

      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend codigo de retorno ${error.status}, ` +
          `El cuerpo fue: ${JSON.parse(JSON.stringify(error.error))}`);
        return Observable.throw (`Error al procesar la solicitud (server error) ${JSON.stringify(error.error)}`);

      }
      // return Observable.throw (`Error al procesar la solicitud: ${error}`);
    }

    private jwt () { // json web token.
        // create authorization header with jwt token
        const currentUser = localStorage.getItem('TOKEN_CAJERO');
        if (currentUser) {
            const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Bearer ' + currentUser
              })
            };
            return httpOptions;
        }
    }
}
