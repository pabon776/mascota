import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { IonicPage, NavController, NavParams,LoadingController,ToastController } from 'ionic-angular';

//import { mascota } from '../mascota/mascota';
import { MascotaPage } from '../mascota/mascota';
import { VacunasPage } from '../vacunas/vacunas';
import { ContactosPage } from '../contactos/contactos';



//*********ERICKA

import { AppSettings } from '../../config/app.settings';
import { HttpProvider } from '../../providers/http/http';

//import { Http, Headers, RequestOptions, Response } from '@angular/http';


//**************ERICKA



/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})
export class InicioPage {

    private var_usuario: string;
  private var_pin: string;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams    
          
                
     ) {
    console.log("holap ");
 

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InicioPage');
  }

        abrirMascota() {
   this.navCtrl.push("MascotaPage");
   //  this.navCtrl.push(InicioPage);
  }
          abrirVacunas() {
   this.navCtrl.push("VacunasPage");
   //  this.navCtrl.push(InicioPage);
   
  }
            abrirContfacto() {
   this.navCtrl.push("ContactosPage");
 }

 public peticionToken (): void {
    // let url: string = AppSettings.IGOB_CIUDADANO + 'api/apiLogin';
    let url: string = AppSettings.EP_VALLE + "api/apiLogin";
    let params: any = {"usr_usuario": "administrador", "usr_clave": "123456"};
    this.httpService.loginResource (url, params).
        subscribe (
              _token => this.ayudaPeticionToken (_token),
              error => {
                alert(error);
              });
  }

  public ayudaPeticionToken (data) {
    console.log("token",data);

  }


}
