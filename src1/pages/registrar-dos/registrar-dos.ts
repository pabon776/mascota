import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CompromimisoPage } from '../compromimiso/compromimiso';

/**
 * Generated class for the RegistrarDosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrar-dos',
  templateUrl: 'registrar-dos.html',
})
export class RegistrarDosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
      abrirCompromiso() {
   this.navCtrl.push("CompromimisoPage");
   //  this.navCtrl.push(InicioPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrarDosPage');
  }

}
