import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompromimisoPage } from './compromimiso';

@NgModule({
  declarations: [
    CompromimisoPage,
  ],
  imports: [
    IonicPageModule.forChild(CompromimisoPage),
  ],
})
export class CompromimisoPageModule {}
