import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';

/**
 * Generated class for the CompromimisoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-compromimiso',
  templateUrl: 'compromimiso.html',
})
export class CompromimisoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


        volverhyInicio() {
   this.navCtrl.push("InicioPage");
   //  this.navCtrl.push(InicioPage);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CompromimisoPage');
  }

}
