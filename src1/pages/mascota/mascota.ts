import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrarUnoPage } from '../registrar-uno/registrar-uno';
import { InicioPage } from '../inicio/inicio';

/**
 * Generated class for the MascotaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mascota',
  templateUrl: 'mascota.html',
})
export class MascotaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
      abrirRegistroUno() {
   this.navCtrl.push("RegistrarUnoPage");
   //  this.navCtrl.push(InicioPage);
  }
        volverInicio() {
   this.navCtrl.push("InicioPage");
   //  this.navCtrl.push(InicioPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MascotaPage');
  }

}
