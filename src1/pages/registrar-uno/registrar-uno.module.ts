import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrarUnoPage } from './registrar-uno';

@NgModule({
  declarations: [
    RegistrarUnoPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrarUnoPage),
  ],
})
export class RegistrarUnoPageModule {}
