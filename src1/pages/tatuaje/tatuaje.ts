import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrarDosPage } from '../registrar-dos/registrar-dos';

/**
 * Generated class for the TatuajePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tatuaje',
  templateUrl: 'tatuaje.html',
})
export class TatuajePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TatuajePage');
  }

      abrirRegistroDos() {
   this.navCtrl.push("RegistrarDosPage");
   //  this.navCtrl.push(InicioPage);
  }

}
