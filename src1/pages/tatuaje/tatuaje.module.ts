import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TatuajePage } from './tatuaje';

@NgModule({
  declarations: [
    TatuajePage,
  ],
  imports: [
    IonicPageModule.forChild(TatuajePage),
  ],
})
export class TatuajePageModule {}
