import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrarDosPage } from '../registrar-dos/registrar-dos';

/**
 * Generated class for the ChipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chip',
  templateUrl: 'chip.html',
})
export class ChipPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChipPage');
  }
      abrirRegistroDos() {
   this.navCtrl.push("RegistrarDosPage");
   //  this.navCtrl.push(InicioPage);
  }
}
